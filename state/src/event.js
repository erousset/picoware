export class Event {
    #name

    constructor(name) {
        this.#name = name;
    }
    get name() {
        return this.#name;
    }
}

/**
 * @param {string} eventName 
 * @returns {Event}
 */
export function raise(eventName) { return new Event(eventName) };