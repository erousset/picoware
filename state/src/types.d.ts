import { Event } from "./event.js"
import { AtomicState } from "./atomicState";
import { ActivityState } from "./activityState"
import { DoneState, ErrorState } from "./finalState"
import { OrState } from "./orState"
import { AndState } from "./andState"
import { Machine } from "./machine.js"

declare namespace picoware {

    type Action = (data?: any) => void;
    type Guard = ((data?: any) => boolean);
    type EventAction = Action | Event;
    type Activity = Promise<any> | (() => Promise<any>);
    type Final = "done" | "error" // doesn't work in JS files.


    interface TransitionConfig {
        target: string
        guard?: Guard
        delay?: number
        actions?: EventAction | EventAction[]
    }

    interface NodeConfig {
        src?: Machine
        enter?: Action | Action[]
        exit?: Action | Action[]
        activity?: Activity
        parallel?: boolean
        initial?: string
        always?: TransitionConfig | TransitionConfig[]
        states?: {
            [key: string]: NodeConfig
        }
        events?: {
            [key: string]: TransitionConfig | TransitionConfig[]
        }
        done?: TransitionConfig | TransitionConfig[]
        error?: TransitionConfig | TransitionConfig[]
        history?: boolean | Guard,
        final?: Final,
        context?: Object
    }
}