import { Observable, Signal } from "@picoware/signal";
import { Machine } from "./machine.js";
import { InternalTransition, ExternalTransition } from "./transition.js";
export class State extends Machine {
    #name;
    #parent;

    #enterActions;
    #exitActions;
    #autoTransitions = [];
    #eventsTransitions = {};
    #pendingTransitionIds = [];

    changed = new Signal();

    constructor(name, parent, enterActions = [], exitActions = [], context = {}) {
        super(context);
        this.#name = name;
        this.#parent = parent;
        this.#enterActions = enterActions;
        this.#exitActions = exitActions;
    }

    // get/set

    get parent() {
        return this.#parent;
    }

    get name() {
        return this.#name;
    }

    set parent(parent) {
        this.#parent = parent;
    }

    get active() {
        if (!this.parent) return true;

        // we can't use instanceof as it causes circular dependancies
        // check OrNode
        if (this.parent.nodes && this.parent.initial) return this.parent.current == this && this.parent.active;

        //check And Node
        if (this.parent.nodes) return this.parent.active;

        throw "Internal error: parent should be either AndNode or OrNode";
    }

    // machine api

    start() {
        this.enter();
    }

    _send(event, data) {
        let accepted = this.execTransitions(this.#eventsTransitions[event], data);
        return accepted;
    }

    // config api

    addAutoTransitions(autoTransitions) {
        this.#autoTransitions = [...this.#autoTransitions, ...autoTransitions];
    }

    addEventTransitions(eventsTransitions) {
        this.#eventsTransitions = { ...this.#eventsTransitions, ...eventsTransitions };
    }

    addEnterActions(enterActions) {
        this.#enterActions = [...enterActions, ...this.#enterActions];
    }

    addExitActions(exitActions) {
        this.#exitActions = [...this.#exitActions, ...exitActions];
    }

    // execution api

    enter() {
        this.#enterActions.forEach((action) => action());
        if (this.#parent) this.parent.current = this; // et si le parent est pas ornode?
        this.changed.emit();
        this.execTransitions(this.#autoTransitions);
    }

    exit() {
        this.cancelPendingTransitions();
        this.#exitActions.forEach((action) => action());
    }

    findTransition(transitions, data) {
        return transitions.find((transition) => !transition.guard || transition.guard(data));
    }

    raise(event, data) {
        let self = this;
        setTimeout(() => self._send(event, data), 0); // push in the event queue
    }

    execInternalTransition(transition, data) {
        transition.actions.forEach((action) => action(data));
    }

    execExternalTransition(transition, data) {
        this.exit();
        transition.actions.forEach((action) => action(data));
        if (this.parent) {
            this.parent.current = transition.target; //allows root transitions
        }
        transition.target.enter();
    }

    execTransition(transition, data) {
        if (transition instanceof InternalTransition) {
            this.execInternalTransition(transition, data);
        }

        if (transition instanceof ExternalTransition) {
            this.execExternalTransition(transition, data);
        }
    }

    execTransitions(transitions, data) {
        if (!transitions) return false;
        let transition = this.findTransition(transitions, data);
        if (!transition) return false;

        if (!transition.delay) {
            this.execTransition(transition, data);
        } else {
            let pendingId = setTimeout(() => {
                this.execTransition(transition, data);
            }, transition.delay);
            this.#pendingTransitionIds.push(pendingId);
        }
        return true;
    }

    cancelPendingTransitions() {
        while (this.#pendingTransitionIds.length > 0) {
            let pendingId = this.#pendingTransitionIds.pop();
            clearTimeout(pendingId);
        }
    }
}
