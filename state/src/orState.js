import { ActorState } from "./actorState.js";
import { Status } from "./status.js";

export class OrState extends ActorState {
    #nodes;
    #initial;
    #current = null;
    #history = null;
    #historyGuard;

    constructor(name, parent, enterActions, exitActions, historyGuard, context) {
        super(name, parent, enterActions, exitActions, context);
        this.#historyGuard = historyGuard;
        this.status.value = new Status(this.state);
        this.changed.on(() => this.status.value = new Status(this.state))
    }

    get current() {
        return this.#current;
    }

    set current(current) {
        this.#current = current;
        this.#history = current;
    }

    get nodes() {
        return this.#nodes;
    }

    getSubNode(name) {
        return this.#nodes.find((node) => node.name == name);
    }

    get historyGuard() {
        return this.#historyGuard;
    }

    set historyGuard(value) {
        this.#historyGuard = value;
    }

    get initial() {
        return this.#initial;
    }

    set nodes(nodes) {
        this.#nodes = nodes;
        nodes.forEach((node) => node.changed.on(() => this.changed.emit()));
    }

    set initial(initial) {
        this.#initial = initial;
    }

    get state() {
        if (!this.#current) return [this.name];
        return this.#current.state.map((state) => `${this.name}.${state}`);
    }

    enter() {
        super.enter();

        // we exited in an auto-transition, so we don't enter child
        if (this.parent instanceof OrState && this.parent.current != this) return;

        if (!this.#historyGuard() || !this.#history) this.current = this.#initial;
        else this.current = this.#history;
        if (this.#current) this.#current.enter();
    }

    exit() {
        if (this.#current) this.#current.exit();
        super.exit();
        this.#current = null; // don't use setter to keep history
    }

    _send(event, data) {
        let accepted = super._send(event, data);
        if (!accepted) {
            accepted = this.#current._send(event, data);
        }
        return accepted;
    }

    reset() {
        this.#history = null;
    }
}
