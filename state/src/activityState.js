import { ActorState } from "./actorState.js";
import { Status } from "./status.js";

export class ActivityState extends ActorState {
    #activity;

    constructor(name, parent, activity, enterActions = [], exitActions = [], context = {}) {
        super(name, parent, enterActions, exitActions, context);
        this.#activity = activity;
        this.status.value = new Status(this.state);
        this.changed.on(() => this.status.value = new Status(this.state))
    }

    get state() {
        return [this.name];
    }

    enter() {
        super.enter();
        this.#activity()
            .then((value) => {
                if (this.active) this._send("done", value);
            })
            .catch((value) => {
                if (this.active) this._send("error", value);
            });
    }
}
