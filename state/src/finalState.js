import { AtomicState } from "./atomicState.js";

export class ErrorState extends AtomicState {
    constructor(name, parent, enterActions = [], exitActions = [], context = {}) {
        super(name, parent, enterActions, exitActions, context);
    }
    enter() {
        super.enter();
        if (this.parent) this.parent._send("error", this.name);
    }
}
export class DoneState extends AtomicState {
    constructor(name, parent, enterActions = [], exitActions = [], context = {}) {
        super(name, parent, enterActions, exitActions, context);
    }
    enter() {
        super.enter();

        if (this.parent) {
            // reset history on done
            if (this.parent.current) this.parent.reset();
            this.parent._send("done", this.name)
        };
    }
}
