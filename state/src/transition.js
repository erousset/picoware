class Transition {
    #guard;
    #actions;
    #delay;

    constructor(guard, actions, delay) {
        this.#guard = guard;
        this.#actions = actions;
        this.#delay = delay;
    }

    get guard() {
        return this.#guard;
    }

    get actions() {
        return this.#actions;
    }

    get delay() {
        return this.#delay;
    }
}

export class InternalTransition extends Transition {}

export class ExternalTransition extends Transition {
    #target;

    constructor(target, guard, actions, delay) {
        super(guard, actions, delay);
        this.#target = target;
    }

    get target() {
        return this.#target;
    }
}
