import { ExternalTransition, InternalTransition } from "./transition.js";
import { AtomicState } from "./atomicState.js";
import { ErrorState, DoneState } from "./finalState.js";
import { ActivityState } from "./activityState.js";
import { OrState } from "./orState.js";
import { AndState } from "./andState.js";
import { ActorState } from "./actorState.js";
import { State } from "./state.js"
import { Event } from './event.js'

const parsingKeys = {
    transition: ["target", "guard", "actions", "delay"],
    node: [
        "enter",
        "exit",
        "activity",
        "states",
        "initial",
        "parallel",
        "always",
        "events",
        "done",
        "error",
        "history",
        "final",
        "context",
    ],
};

export class Parser {
    static checkStateConfig(config) {
        let unknownKeys = Object.keys(config).filter((key) => !parsingKeys.node.includes(key));
        if (unknownKeys.length > 0) throw `option(s) ${unknownKeys} is/are invalid`;
    }

    static checkTransitionConfig(config) {
        let unknownKeys = Object.keys(config).filter((key) => !parsingKeys.transition.includes(key));
        if (unknownKeys.length > 0) throw `option(s) ${unknownKeys} is/are invalid`;
    }
    static getActions(config, node) {
        // actions can be undefined
        if (!config) return [];

        // actions can be a single value
        let configs = config;
        if (!(config instanceof Array)) configs = [config];

        // generate raise actions
        configs = configs.map(config => {
            if (config instanceof Event) {
                if (!node) throw `Can't raise event ${config.name}in enter/exit actions. Put it in a transition instead.`
                // return raise action
                return () => node.parent.raise(config.name);
            }
            return config
        })

        return configs;
    }

    static getGuard(config) {
        // guard can be undefined
        if (!config) return null;

        // guard is defined
        return config;
    }

    static getTransition(config, node, nodes) {
        Parser.checkTransitionConfig(config);
        let delay = config.delay ? config.delay : 0;

        if (config.target == "internal") {
            return new InternalTransition(Parser.getGuard(config.guard), Parser.getActions(config.actions, node), delay);
        }

        if (config.target == "self") {
            return new ExternalTransition(node, Parser.getGuard(config.guard), Parser.getActions(config.actions, node), delay);
        }

        let target = nodes.find((node) => node.name == config.target);
        if (!target) throw `target subnode ${config.target} is not defined`;
        if (target == node) throw `Use either "self" or "internal" target for self transitions`;
        return new ExternalTransition(target, Parser.getGuard(config.guard), Parser.getActions(config.actions, node), delay);
    }

    static getTransitions(configs, node, nodes) {
        // transitions can be undefined
        if (!configs) return [];

        // transition can be a single value so encapsulate
        if (!(configs instanceof Array)) configs = [configs];

        // compute transitions
        return configs.map((config) => Parser.getTransition(config, node, nodes));
    }

    static getEventTransitions(config, node, nodes) {
        // events can be undefined
        if (!config) return {};

        // compute events
        let events = {};
        Object.keys(config).forEach((eventName) => {
            let transitionsConfig = config[eventName];
            let transitions = Parser.getTransitions(transitionsConfig, node, nodes);
            events[eventName] = transitions;
        });
        return events;
    }

    static getActivity(config) {
        // activity can be undefined
        if (!config) return null;
        return config;
    }

    static getInitial(config, nodes) {
        if (!config) throw `Initial state is not defined`;
        let node = nodes.find((node) => node.name == config);
        if (!node) throw `Initial state ${config} doesn't exist.`;
        return node;
    }

    static getHistoryGuard(config) {
        if (!config) return () => false;
        if (typeof config == "function") return config;
        if (typeof config == "boolean") return () => true;
        throw "History should be either a boolean or a guard function";
    }

    static getAtomicState(name, parent, config) {
        if (config.states) throw `Atomic node ${name} can't have subnodes`;
        if (config.initial) throw `Atomic node ${name} can't have initial subnode`;
        if (config.history) throw `Atomic node ${name} can't have history`;
        if (config.parallel) throw `Atomic node ${name} can't be a parallel node.`;
        if (config.done) throw `Atomic node ${name} can't have done transitions`;
        if (config.error) throw `Atomic node ${name} can't have error transitions`;

        let node = new AtomicState(name, parent, Parser.getActions(config.enter), Parser.getActions(config.exit), config.context);

        // transition should be defined by the parent
        // root node can only have self transitions (internal or external)
        if (!node.parent) {
            node.addAutoTransitions(Parser.getTransitions(config.always, node, [node]));
            node.addEventTransitions(Parser.getEventTransitions(config.events, node, [node]));
        }
        return node;
    }

    static getErrorState(name, parent, config) {
        if (config.states) throw `Error node ${name} can't have subnodes`;
        if (config.initial) throw `Error node ${name} can't have initial subnode`;
        if (config.history) throw `Error node ${name} can't have history`;
        if (config.parallel) throw `Error node ${name} can't be a parallel node.`;
        if (config.done) throw `Error node ${name} can't have done transitions`;
        if (config.error) throw `Error node ${name} can't have error transitions`;

        let node = new ErrorState(name, parent, Parser.getActions(config.enter), Parser.getActions(config.exit), config.context);

        // transition should be defined by the parent
        // root node can only have self transitions (internal or external)
        if (!node.parent) {
            node.addAutoTransitions(Parser.getTransitions(config.always, node, [node]));
            node.addEventTransitions(Parser.getEventTransitions(config.events, node, [node]));
        }
        return node;
    }

    static getDoneState(name, parent, config) {
        if (config.states) throw `Done node ${name} can't have subnodes`;
        if (config.initial) throw `Done node ${name} can't have initial subnode`;
        if (config.history) throw `Done node ${name} can't have history`;
        if (config.parallel) throw `Done node ${name} can't be a parallel node.`;
        if (config.done) throw `Done node ${name} can't have done transitions`;
        if (config.error) throw `Done node ${name} can't have error transitions`;

        let node = new DoneState(name, parent, Parser.getActions(config.enter), Parser.getActions(config.exit), config.context);

        // transition should be defined by the parent
        // root node can only have self transitions (internal or external)
        if (!node.parent) {
            node.addAutoTransitions(Parser.getTransitions(config.always, node, [node]));
            node.addEventTransitions(Parser.getEventTransitions(config.events, node, [node]));
        }
        return node;
    }

    static getActivityState(name, parent, config) {
        if (!config.activity) throw `Activity node  ${name} activity property is undefined.`;
        if (config.states) throw `Activity node ${name} can't have subnodes`;
        if (config.initial) throw `Activity node ${name} can't have initial subnode`;
        if (config.history) throw `Activity node ${name} can't have history`;
        if (config.parallel) throw `Activity node ${name} can't be a parallel node.`;

        let node = new ActivityState(
            name,
            parent,
            Parser.getActivity(config.activity),
            Parser.getActions(config.enter),
            Parser.getActions(config.exit),
            config.context
        );

        // transition should be defined by the parent
        // root node can only have self transitions (internal or external)
        if (!node.parent) {
            node.addAutoTransitions(Parser.getTransitions(config.always, node, [node]));
            node.addEventTransitions(Parser.getEventTransitions(config.events, node, [node]));
            node.addDoneTransitions(Parser.getTransitions(config.done, node, [node]));
            node.addErrorTransitions(Parser.getTransitions(config.error, node, [node]));
        }
        return node;
    }

    static getAndState(name, parent, config) {
        if (!config.parallel) throw `parallel node  ${name} parallel property is undefined`;
        if (config.initial) throw `parallel node  ${name} can't have initial subnode`;
        if (config.history) throw `parallel node   ${name} can't have history`;

        let node = new AndState(name, parent, Parser.getActions(config.enter), Parser.getActions(config.exit), config.context);
        if (!config.states) throw `parallel node  ${name} should have subnodes`;
        let subnodes = Object.keys(config.states).map((name) => {
            let cfg = config.states[name];
            let subNode = cfg.src;

            if (!subNode) return Parser.getState(name, node, cfg);
            if (!(subNode instanceof State)) throw `${name}.src should be a node instance.`;
            if (name != subNode.name) throw `external node ${subNode.name} doesn't match name ${name}`;

            subNode.parent = node;
            subNode.addEnterActions(Parser.getActions(cfg.enter));
            subNode.addExitActions(Parser.getActions(cfg.exit));
            return subNode;
        });
        subnodes.forEach((node) => {
            let cfg = config.states[node.name];
            // allow only self transitions
            node.addAutoTransitions(Parser.getTransitions(cfg.always, node, [node]));
            node.addEventTransitions(Parser.getEventTransitions(cfg.events, node, [node]));
            if (node instanceof ActorState) {
                node.addErrorTransitions(Parser.getTransitions(cfg.error, node, [node]));
                node.addDoneTransitions(Parser.getTransitions(cfg.done, node, [node]));
            }
            if (node instanceof OrState && cfg.history) {
                node.historyGuard = Parser.getHistoryGuard(cfg.history)
            }
        });

        node.nodes = subnodes;
        // transition should be defined by the parent
        // root node can only have self transitions (internal or external)
        if (!node.parent) {
            node.addAutoTransitions(Parser.getTransitions(config.always, node, [node]));
            node.addEventTransitions(Parser.getEventTransitions(config.events, node, [node]));
            node.addDoneTransitions(Parser.getTransitions(config.done, node, [node]));
            node.addErrorTransitions(Parser.getTransitions(config.error, node, [node]));
        }
        return node;
    }

    static getOrState(name, parent, config) {
        if (!config.states) throw `orthogonal node ${name} states property is undefined.`;
        if (config.parallel) throw `orthogonal node ${name} can't be parallel.`;

        let node = new OrState(
            name,
            parent,
            Parser.getActions(config.enter),
            Parser.getActions(config.exit),
            Parser.getHistoryGuard(config.history),
            config.context,
        );
        if (!config.states) throw `orthogonal node ${name} should have subnodes`;
        let subnodes = Object.keys(config.states).map((name) => {
            let cfg = config.states[name];
            let subNode = cfg.src;

            if (!subNode) return Parser.getState(name, node, cfg);
            if (!(subNode instanceof State)) throw `${name}.src should be a node instance.`;
            if (name != subNode.name) throw `external node ${subNode.name} doesn't match name ${name}`;

            subNode.parent = node;
            subNode.addEnterActions(Parser.getActions(cfg.enter));
            subNode.addExitActions(Parser.getActions(cfg.exit));
            return subNode;
        });
        subnodes.forEach((node) => {
            let cfg = config.states[node.name];
            node.addAutoTransitions(Parser.getTransitions(cfg.always, node, subnodes));
            node.addEventTransitions(Parser.getEventTransitions(cfg.events, node, subnodes));
            if (node instanceof ActorState) {
                node.addErrorTransitions(Parser.getTransitions(cfg.error, node, subnodes));
                node.addDoneTransitions(Parser.getTransitions(cfg.done, node, subnodes));
            }
            if (node instanceof OrState && cfg.history) {
                node.historyGuard = Parser.getHistoryGuard(cfg.history)
            }
        });

        node.nodes = subnodes;
        node.initial = Parser.getInitial(config.initial, subnodes);

        // transition should be defined by the parent
        // root node can only have self transitions (internal or external)
        if (!node.parent) {
            node.addAutoTransitions(Parser.getTransitions(config.always, node, [node]));
            node.addEventTransitions(Parser.getEventTransitions(config.events, node, [node]));
            node.addDoneTransitions(Parser.getTransitions(config.done, node, [node]));
            node.addErrorTransitions(Parser.getTransitions(config.error, node, [node]));
        }
        return node;
    }

    static getState(name, parent, config) {
        Parser.checkStateConfig(config);
        if (name == "DONE" || config.final == "done") return Parser.getDoneState(name, parent, config);
        if (name == "ERROR" || config.final == "error") return Parser.getErrorState(name, parent, config);
        if (config.hasOwnProperty("activity")) return Parser.getActivityState(name, parent, config);
        if (config.hasOwnProperty("parallel")) return Parser.getAndState(name, parent, config);
        if (config.hasOwnProperty("states")) return Parser.getOrState(name, parent, config);

        return Parser.getAtomicState(name, parent, config);
    }
}
