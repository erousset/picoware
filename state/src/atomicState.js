import { State } from "./state.js";
import { Status } from "./status.js";

export class AtomicState extends State {

    constructor(name, parent, enterActions = [], exitActions = [], context = {}) {
        super(name, parent, enterActions, exitActions, context);
        this.status.value = new Status(this.state);
        this.changed.on(() => this.status.value = new Status(this.state))
    }

    get state() {
        return [this.name];
    }

    onDone(callback) {
        throw "OnDone is not defined on AtomicState"
    }

    onError(callback) {
        throw "OnError is not defined on AtomicState"
    }
}
