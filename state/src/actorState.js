import { Observable, Signal } from "@picoware/signal";
import { State } from "./state.js";

export class ActorState extends State {
    #errorTransitions = [];
    #doneTransitions = [];

    done = new Observable(false);
    error = new Observable(false);

    #onError = new Signal();
    #onDone = new Signal();

    constructor(name, parent, enterActions, exitActions, context) {
        super(name, parent, enterActions, exitActions, context)
        this.done.changed.on((value) => { if (value) this.#onDone.emit() })
        this.error.changed.on((value) => { if (value) this.#onError.emit() })
    }

    addErrorTransitions(errorTransitions) {
        this.#errorTransitions = [...this.#errorTransitions, ...errorTransitions];
    }

    addDoneTransitions(doneTransitions) {
        this.#doneTransitions = [...this.#doneTransitions, ...doneTransitions];
    }

    enter() {
        this.done.value = false;
        this.error.value = false;

        super.enter();
    }

    _send(event, data) {
        switch (event) {
            case "done":
                this.done.value = true;
                return this.execTransitions(this.#doneTransitions, data);
            case "error":
                this.error.value = true;
                return this.execTransitions(this.#errorTransitions, data);
            default:
                return super._send(event, data);
        }
    }

    // machine api

    onError(callback) {
        return this.#onError.on(callback);
    }

    onDone(callback) {
        return this.#onDone.on(callback);
    }
}
