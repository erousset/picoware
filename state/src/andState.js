import { ActorState } from "./actorState.js";
import { Status } from "./status.js";

export class AndState extends ActorState {
    #nodes = [];
    #actorNodes = [];

    constructor(name, parent, enterActions, exitActions, context = {}) {
        super(name, parent, enterActions, exitActions, context);
        this.status.value = new Status(this.state);
        this.changed.on(() => this.status.value = new Status(this.state))
    }

    get nodes() {
        return this.#nodes;
    }

    set nodes(nodes) {
        this.#nodes = nodes;
        this.#actorNodes = nodes.filter((node) => node instanceof ActorState);
        this.#nodes.forEach((node) => node.changed.on(() => this.changed.emit()));
        this.#actorNodes.forEach((node) => node.done.changed.on(() => this.processDone()));
        this.#actorNodes.forEach((node) => node.error.changed.on(() => this.processError()));
    }

    get state() {
        return this.#nodes
            .map((node) => node.state)
            .flat()
            .map((state) => `${this.name}.${state}`);
    }

    enter() {
        super.enter();
        this.#nodes.forEach((node) => node.enter());
    }
    exit() {
        this.#nodes.forEach((node) => node.exit());
        super.exit();
    }

    _send(event, data) {
        let accepted = super._send(event, data);
        if (!accepted) {
            accepted = this.#nodes.map((node) => node._send(event, data)).reduce((acc, accepted) => acc || accepted);
        }
        return accepted;
    }

    processDone() {
        let done = this.#actorNodes.reduce((acc, node) => acc && node.done.value, true);
        if (done) {
            this.done.value = true;
            this._send("done");
        }
    }

    processError() {
        let error = this.#actorNodes.reduce((acc, node) => acc || node.error.value, true);
        if (error) {
            this.error.value = true;
            this._send("error");
        }
    }
}
