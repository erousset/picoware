import { Status } from "./status.js";
import { Observable } from "@picoware/signal";

export class Machine {

    #context;
    status = new Observable(new Status([]));
    _transitionning = false;
    strict = false;

    constructor(context) {
        this.#context = context;
    }

    get context() {
        return this.#context;
    }

    get state() {
        return [] // must be implemented in subclass
    }

    start() {
        throw "Start must be implemented in subclass"
    }

    onDone(callback) {
        throw "OnDone must be implemented in subclass"
    }

    onError(callback) {
        throw "OnError must be implemented in subclass"
    }

    _send(event, data) {
        throw "_send must be implemented in subclass"
    }

    send(event, data = null) {
        if (!this._transitionning) { // ignore the message if the current transition in not finished
            this._transitionning = true;
            let accepted = this._send(event, data);
            this._transitionning = false;
            return accepted;
        }
        else {
            if (this.strict) throw "@picoware/state: can't start a new transition until the previous one is finished "
            return false;
        }
    }

    matches(stateDescriptor) {
        return this.status.value.matches(stateDescriptor);
    }

    wait(stateDescriptor, timeout = 1000) {
        return new Promise((resolve, reject) => {
            if (this.status.value.matches(stateDescriptor)) {
                resolve();
            } else {
                let timeoutID = setTimeout(() => {
                    reject(`couldn't reach ${stateDescriptor} under ${timeout}ms`);
                }, timeout);
                let unbind = {};
                unbind.func = this.status.onChanged(() => {
                    if (this.status.value.matches(stateDescriptor)) {
                        clearTimeout(timeoutID);
                        unbind.func();
                        resolve();
                    }
                });
            }
        });
    }
}