import { Parser } from "./parser.js";
import { raise } from "./event.js";
import { Machine } from "./machine.js";

/**
 * @param {string} name 
 * @param {import("./types").picoware.NodeConfig} config 
 * @returns {import("./machine").Machine} 
 */
let parseMachine = (name, config) => Parser.getState(name, null, config);

export { parseMachine, Machine, raise };
