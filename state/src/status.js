
export class Status {
    #state = [];
    constructor(state) {
        this.#state = state;
    }

    get state() {
        return this.#state;
    }

    matches(stateDescriptor) {
        const regExp = new RegExp(stateDescriptor);
        return this.#state.some((stateString) => regExp.test(stateString));
    }
}