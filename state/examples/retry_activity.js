import { StateMachine, parseMachine } from '../src/exports.js';

const activities = {
    calculate: () => Promise.resolve()// return  instead a promise whose resolution depends on the context
}

const rootNode = parseMachine("root", {
    initial: 'COMPUTING',
    states: {
        COMPUTING: {
            done: { target: "RESULT" },
            error: { target: "self" }, // will re-trigger computations until we get a result

            activity: activities.calculate
        },
        RESULT: {}
    }
});

const machine = new StateMachine(rootNode);
machine.changed.on(() => console.log(machine.state))

machine.start();
// [ 'root' ]
// [ 'root.COMPUTING' ]
// [ 'root.RESULT' ]

