import { StateMachine, parseMachine } from '../src/exports.js';

const rootNode = parseMachine("root", {
    parallel: true,
    states: {
        BOLD: {
            initial: 'OFF',
            states: {
                ON: {
                    events: {
                        TOGGLE_BOLD: { target: "OFF" }
                    }
                },
                OFF: {
                    events: {
                        TOGGLE_BOLD: { target: "ON" }
                    }
                }
            }
        },
        UNDERLINE: {
            initial: 'OFF',
            states: {
                ON: {
                    events: {
                        TOGGLE_UNDERLINE: { target: "OFF" }
                    }
                },
                OFF: {
                    events: {
                        TOGGLE_UNDERLINE: { target: "ON" }
                    }
                }
            }
        },
        ITALICS: {
            initial: 'OFF',
            states: {
                ON: {
                    events: {
                        TOGGLE_ITALICS: { target: "OFF" }
                    }
                },
                OFF: {
                    events: {
                        TOGGLE_ITALICS: { target: "ON" }
                    }
                }
            }
        },
    }
});

const machine = new StateMachine(rootNode);
machine.changed.on(() => console.log(machine.state))

machine.start();
// [ 'root.BOLD', 'root.UNDERLINE', 'root.ITALICS' ]
// [ 'root.BOLD.OFF', 'root.UNDERLINE', 'root.ITALICS' ]
// [ 'root.BOLD.OFF', 'root.UNDERLINE.OFF', 'root.ITALICS' ]
// [ 'root.BOLD.OFF', 'root.UNDERLINE.OFF', 'root.ITALICS.OFF' ]

machine.send('TOGGLE_BOLD');
// [ 'root.BOLD.ON', 'root.UNDERLINE.OFF', 'root.ITALICS.OFF' ]

machine.send('TOGGLE_UNDERLINE');
// [ 'root.BOLD.ON', 'root.UNDERLINE.OFF', 'root.ITALICS.OFF' ]

machine.send('TOGGLE_ITALICS');
// [ 'root.BOLD.ON', 'root.UNDERLINE.ON', 'root.ITALICS.ON' ]


