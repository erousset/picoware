
import { StateMachine, parseMachine } from '../src/exports.js';

const guards = {
    isRaining: () => true, // return something that depends on the context
    isHot: () => false
}

const rootNode = parseMachine("root", {
    initial: 'GOING_OUT',
    states: {
        GOING_OUT: {
            events: {
                CHECK_WEATHER: [
                    { target: "TAKING_UMBRELLA", guard: guards.isRaining }, // guard is true so we take this one
                    { target: "TAKING_HAT", guard: guards.isHot }
                ]
            }
        },
        TAKING_HAT: {},
        TAKING_UMBRELLA: {},

    }
});

const machine = new StateMachine(rootNode);
machine.changed.on(() => console.log(machine.state))

machine.start();
// [ 'root' ]
// [ 'root.GOING_OUT' ]

machine.send('CHECK_WEATHER');
// [ 'root.TAKING_UMBRELLA' ]