
import { StateMachine, parseMachine } from '../src/exports.js';

const actions = {
    enterON: () => console.log("enter ON"),
    exitON: () => console.log("exit ON"),
    enterOFF: () => console.log("enter OFF"),
    exitOFF: () => console.log("exit OFF"),
    toON: () => console.log("to ON"),
    toOFF: () => console.log("to OFF")
}

const rootNode = parseMachine("root", {
    initial: 'OFF',
    states: {
        ON: {
            enter: actions.enterON, // enter actions (can be an array)
            exit: actions.exitON, // exit actions (can be an array)
            events: {
                TOGGLE: { target: "OFF", actions: actions.toOFF } // transition actions (can be an array)
            }
        },
        OFF: {
            enter: actions.enterOFF,
            exit: actions.exitOFF,
            events:
            {
                TOGGLE: { target: "ON", actions: actions.toON }
            }
        }
    }
});

// Machine instance 
const machine = new StateMachine(rootNode);

// Add listener
machine.changed.on(() => console.log(machine.state))

// Start machine
machine.start();
// [ 'root' ]
// enter OFF
// [ 'root.OFF' ]

machine.send('TOGGLE');
// exit OFF
// to ON
// enter ON
// [ 'root.ON' ]

machine.send('TOGGLE');
// exit ON
// to OFF
// enter OFF
// [ 'root.OFF' ]