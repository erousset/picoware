import { StateMachine, parseMachine } from '../src/exports.js';

const rootNode = parseMachine("root", {
    initial: 'OFF',
    states: {
        ON: {
            history: true, // history activation
            events: {
                TOGGLE: { target: "OFF" }
            },

            initial: "PLAYING",
            states: {
                PLAYING: {
                    events: {
                        PAUSE: { target: "PAUSED" }
                    }
                },
                PAUSED: {
                    events: {
                        PLAY: { target: "PLAYING" }
                    }
                }
            }
        },
        OFF: {
            events:
            {
                TOGGLE: { target: "ON" }
            }
        }
    }
});

const machine = new StateMachine(rootNode);
machine.changed.on(() => console.log(machine.state))

machine.start();
// [ 'root' ]
// [ 'root.OFF' ]

machine.send('TOGGLE');
// [ 'root.ON' ]
// [ 'root.ON.PLAYING' ]

machine.send('PAUSE');
// [ 'root.ON.PAUSED' ]

machine.send('TOGGLE');
// [ 'root.OFF' ]

machine.send('TOGGLE');
// [ 'root.ON' ]
// [ 'root.ON.PAUSED' ]

