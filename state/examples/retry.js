import { StateMachine, parseMachine } from '../src/exports.js';

const rootNode = parseMachine("root", {
    initial: 'COMPUTE',
    states: {
        COMPUTE: {
            done: { target: "RESULT" },
            error: { target: "self" }, // will re-trigger computations until we get a result

            initial: "CALCULATE",
            states: {
                CALCULATE: {
                    events: {
                        SUCCEED: { target: "DONE" },
                        FAIL: { target: "ERROR" },
                    }
                },
                ERROR: {},
                DONE: {},
            }
        },
        RESULT: {}
    }
});

const machine = new StateMachine(rootNode);
machine.changed.on(() => console.log(machine.state))

machine.start();
// [ 'root' ]
// [ 'root.COMPUTE' ]
// [ 'root.COMPUTE.CALCULATE' ]

machine.send('SUCCEED');
// [ 'root.COMPUTE.DONE' ]
// [ 'root.RESULT' ]


