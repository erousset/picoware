import { StateMachine, parseMachine } from '../src/exports.js';

const actions = {
    enterON: () => console.log("enter ON"),
    exitON: () => console.log("exit ON"),
    enterOFF: () => console.log("enter OFF"),
    exitOFF: () => console.log("exit OFF"),
    enterPLAYING: () => console.log("enter PLAYING"),
    exitPLAYING: () => console.log("exit PLAYING"),
    enterPAUSED: () => console.log("enter PAUSED"),
    exitPAUSED: () => console.log("exit PAUSED"),
    toON: () => console.log("to ON"),
    toOFF: () => console.log("to OFF"),
    play: () => console.log("play"),
    pause: () => console.log("pause"),
}

const rootNode = parseMachine("root", {
    initial: 'OFF',
    states: {
        ON: {
            enter: actions.enterON,
            exit: actions.exitON,
            events: {
                TOGGLE: { target: "OFF", actions: actions.toOFF }
            },
            initial: "PLAYING",
            states: {
                PLAYING: {
                    enter: actions.enterPLAYING,
                    exit: actions.exitPLAYING,
                    events: {
                        PAUSE: { target: "PAUSED", actions: actions.pause }
                    }
                },
                PAUSED: {
                    enter: actions.enterPAUSED,
                    exit: actions.exitPAUSED,
                    events: {
                        PLAY: { target: "PLAYING", actions: actions.play }
                    }
                }
            }
        },
        OFF: {
            enter: actions.enterOFF,
            exit: actions.exitOFF,
            events:
            {
                TOGGLE: { target: "ON", actions: actions.toON }
            }
        }
    }
});

// Machine instance 
const machine = new StateMachine(rootNode);

// Add listener
machine.changed.on(() => console.log(machine.state))

// Start machine
machine.start();
// [ 'root' ]
// enter OFF
// [ 'root.OFF' ]

machine.send('TOGGLE');
// exit OFF
// to ON
// enter ON
// [ 'root.ON' ]
// enter PLAYING
// [ 'root.ON.PLAYING' ]

machine.send('PAUSE');
// exit PLAYING
// pause
// enter PAUSED
// [ 'root.ON.PAUSED' ]

machine.send('TOGGLE');
// exit PAUSED
// exit ON
// to OFF
// enter OFF
// [ 'root.OFF' ]
