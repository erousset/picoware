import { StateMachine, parseMachine, raise } from '../src/exports.js';

const actions = {
    doStuffA: () => console.log("Do stuff A"),
    doStuffB: () => console.log("Do stuff B"),
}

const rootNode = parseMachine("root", {
    parallel: true,
    states: {

        STATE_A: {
            events: {
                DO_STUFF_A: {
                    target: "self",
                    actions: [
                        actions.doStuffA,
                        raise("DO_STUFF_B"), // here we raise a new event on the parent
                    ]
                }
            }
        },

        STATE_B: {
            events: {
                DO_STUFF_B: { target: "self", actions: actions.doStuffB },
            }
        },
    }
});

const machine = new StateMachine(rootNode);
machine.changed.on(() => console.log(machine.state))

machine.start();
// ['root.STATE_A', 'root.STATE_B']

machine.send('DO_STUFF_A');
// Do stuff A
// [ 'root.STATE_A', 'root.STATE_B' ]
// Do stuff B
// [ 'root.STATE_A', 'root.STATE_B' ]



