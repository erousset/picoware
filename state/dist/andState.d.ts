export class AndState extends ActorState {
    constructor(name: any, parent: any, enterActions: any, exitActions: any, context?: {});
    set nodes(arg: any[]);
    get nodes(): any[];
    get state(): string[];
    processDone(): void;
    processError(): void;
    #private;
}
import { ActorState } from "./actorState.js";
//# sourceMappingURL=andState.d.ts.map