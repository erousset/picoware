export class OrState extends ActorState {
    constructor(name: any, parent: any, enterActions: any, exitActions: any, historyGuard: any, context: any);
    set current(arg: any);
    get current(): any;
    set nodes(arg: any);
    get nodes(): any;
    getSubNode(name: any): any;
    set historyGuard(arg: any);
    get historyGuard(): any;
    set initial(arg: any);
    get initial(): any;
    get state(): any;
    reset(): void;
    #private;
}
import { ActorState } from "./actorState.js";
//# sourceMappingURL=orState.d.ts.map