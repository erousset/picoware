export class Status {
    constructor(state: any);
    get state(): any[];
    matches(stateDescriptor: any): boolean;
    #private;
}
//# sourceMappingURL=status.d.ts.map