/**
 * @param {string} name
 * @param {import("./types").picoware.NodeConfig} config
 * @returns {import("./machine").Machine}
 */
export function parseMachine(name: string, config: import("./types").picoware.NodeConfig): import("./machine").Machine;
import { Machine } from "./machine.js";
import { raise } from "./event.js";
export { Machine, raise };
//# sourceMappingURL=exports.d.ts.map