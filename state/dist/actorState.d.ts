export class ActorState extends State {
    constructor(name: any, parent: any, enterActions: any, exitActions: any, context: any);
    done: any;
    error: any;
    addErrorTransitions(errorTransitions: any): void;
    addDoneTransitions(doneTransitions: any): void;
    onError(callback: any): any;
    onDone(callback: any): any;
    #private;
}
import { State } from "./state.js";
//# sourceMappingURL=actorState.d.ts.map