export class Parser {
    static checkStateConfig(config: any): void;
    static checkTransitionConfig(config: any): void;
    static getActions(config: any, node: any): any;
    static getGuard(config: any): any;
    static getTransition(config: any, node: any, nodes: any): InternalTransition | ExternalTransition;
    static getTransitions(configs: any, node: any, nodes: any): any;
    static getEventTransitions(config: any, node: any, nodes: any): {};
    static getActivity(config: any): any;
    static getInitial(config: any, nodes: any): any;
    static getHistoryGuard(config: any): any;
    static getAtomicState(name: any, parent: any, config: any): AtomicState;
    static getErrorState(name: any, parent: any, config: any): ErrorState;
    static getDoneState(name: any, parent: any, config: any): DoneState;
    static getActivityState(name: any, parent: any, config: any): ActivityState;
    static getAndState(name: any, parent: any, config: any): AndState;
    static getOrState(name: any, parent: any, config: any): OrState;
    static getState(name: any, parent: any, config: any): ActivityState | AndState | AtomicState | OrState;
}
import { InternalTransition } from "./transition.js";
import { ExternalTransition } from "./transition.js";
import { AtomicState } from "./atomicState.js";
import { ErrorState } from "./finalState.js";
import { DoneState } from "./finalState.js";
import { ActivityState } from "./activityState.js";
import { AndState } from "./andState.js";
import { OrState } from "./orState.js";
//# sourceMappingURL=parser.d.ts.map