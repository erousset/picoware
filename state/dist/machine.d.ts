export class Machine {
    constructor(context: any);
    status: any;
    _transitionning: boolean;
    strict: boolean;
    get context(): any;
    get state(): any[];
    start(): void;
    onDone(callback: any): void;
    onError(callback: any): void;
    _send(event: any, data: any): void;
    send(event: any, data?: any): false | void;
    matches(stateDescriptor: any): any;
    wait(stateDescriptor: any, timeout?: number): Promise<any>;
    #private;
}
//# sourceMappingURL=machine.d.ts.map