export class InternalTransition extends Transition {
}
export class ExternalTransition extends Transition {
    constructor(target: any, guard: any, actions: any, delay: any);
    get target(): any;
    #private;
}
declare class Transition {
    constructor(guard: any, actions: any, delay: any);
    get guard(): any;
    get actions(): any;
    get delay(): any;
    #private;
}
export {};
//# sourceMappingURL=transition.d.ts.map