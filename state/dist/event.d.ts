/**
 * @param {string} eventName
 * @returns {Event}
 */
export function raise(eventName: string): Event;
export class Event {
    constructor(name: any);
    get name(): any;
    #private;
}
//# sourceMappingURL=event.d.ts.map