export class State extends Machine {
    constructor(name: any, parent: any, enterActions?: any[], exitActions?: any[], context?: {});
    changed: any;
    set parent(arg: any);
    get parent(): any;
    get name(): any;
    get active(): any;
    _send(event: any, data: any): boolean;
    addAutoTransitions(autoTransitions: any): void;
    addEventTransitions(eventsTransitions: any): void;
    addEnterActions(enterActions: any): void;
    addExitActions(exitActions: any): void;
    enter(): void;
    exit(): void;
    findTransition(transitions: any, data: any): any;
    raise(event: any, data: any): void;
    execInternalTransition(transition: any, data: any): void;
    execExternalTransition(transition: any, data: any): void;
    execTransition(transition: any, data: any): void;
    execTransitions(transitions: any, data: any): boolean;
    cancelPendingTransitions(): void;
    #private;
}
import { Machine } from "./machine.js";
//# sourceMappingURL=state.d.ts.map