import { Test } from "@picoware/check";
import assert from "node:assert/strict";
import { parseMachine } from "../src/exports.js";

const test = Test.fromUrl(import.meta.url);

test.add("Error Node: Fail if event transition is defined", () => {
    assert.throws(() =>
        parseMachine("root", {
            initial: "A",
            parallel: true,
            states: {
                A: {},
                ERROR: {
                    events: {
                        TO_A: { target: "A" },
                    },
                },
            },
        })
    );
});

test.add("Error Node: Fail if always transition is defined", () => {
    assert.throws(() =>
        parseMachine("root", {
            initial: "A",
            parallel: true,
            states: {
                A: {},
                ERROR: {
                    always: { target: "A" },
                },
            },
        })
    );
});

test.add("Error Node: Fail if history is defined", () => {
    assert.throws(() =>
        parseMachine("root", {
            initial: "A",
            parallel: true,
            states: {
                A: {},
                ERROR: {
                    history: true,
                },
            },
        })
    );
});

test.run();
