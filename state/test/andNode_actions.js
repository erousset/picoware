import { Test } from "@picoware/check";
import assert from "node:assert/strict";
import { parseMachine, raise } from "../src/exports.js";

const test = Test.fromUrl(import.meta.url);

test.add("enter actions order", () => {
    const log = [];

    const root = parseMachine("root", {
        enter: () => log.push("enter root"),
        parallel: true,
        states: {
            A: { enter: () => log.push("enter A") },
            B: { enter: () => log.push("enter B") },
            C: { enter: () => log.push("enter C") },
        },
    });

    const machine = root;

    machine.start();
    assert.deepEqual(log, ["enter root", "enter A", "enter B", "enter C"]);
});

test.add("exit actions order", () => {
    const log = [];

    const root = parseMachine("root", {
        initial: "BASE",
        states: {
            BASE: {
                exit: () => log.push("exit root"),
                events: {
                    EXIT: {
                        target: "END",
                    },
                },
                parallel: true,
                states: {
                    A: { exit: () => log.push("exit A") },
                    B: { exit: () => log.push("exit B") },
                    C: { exit: () => log.push("exit C") },
                },
            },
            END: {},
        },
    });

    const machine = root;

    machine.start();
    machine.send("EXIT");
    assert.deepEqual(log, ["exit A", "exit B", "exit C", "exit root"]);
});

test.add("raise event", async () => {
    const log = [];
    const root = parseMachine("root", {
        parallel: true,
        events: {
            RAISED: { target: "self", actions: () => log.push("RAISED") }
        },
        states: {
            A: {},
            B: {
                events: {
                    RAISE: {
                        target: "self",
                        actions: raise("RAISED")
                    }
                }
            },
        },
    });
    const machine = root;

    machine.start();
    machine.send("RAISE");
    await new Promise((resolve) => setTimeout(() => resolve(), 0)) // wait until next tick
    assert.deepEqual(log, ["RAISED"]);
})

test.add("composed: raise event", async () => {
    const log = [];

    const B = parseMachine("B", {
        events: {
            RAISE: {
                target: "self",
                actions: raise("RAISED")
            }
        }
    })

    const root = parseMachine("root", {
        parallel: true,
        events: {
            RAISED: { target: "self", actions: () => log.push("RAISED") }
        },
        states: {
            A: {},
            B: {
                src: B
            },
        },
    });
    const machine = root;

    machine.start();
    machine.send("RAISE");
    await new Promise((resolve) => setTimeout(() => resolve(), 0)) // wait until next tick
    assert.deepEqual(log, ["RAISED"]);
})

test.add("composed: enter/exit actions defined inside and outside", () => {
    const log = [];

    const C = parseMachine("C", {
        enter: () => log.push("enter INSIDE"),
        exit: () => log.push("exit INSIDE"),
        parallel: true,
        states: {
            E: {},
            D: {},
        },
    });

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                states: {
                    B: {
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        src: C,
                        enter: () => log.push("enter OUTSIDE"),
                        exit: () => log.push("exit OUTSIDE"),
                        events: {
                            TO_B: { target: "B" },
                        },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    machine.send("TO_C");
    machine.send("TO_B");
    assert.deepEqual(log, ["enter OUTSIDE", "enter INSIDE", "exit INSIDE", "exit OUTSIDE"]);
});

test.add("composed: events actions defined inside and outside", () => {
    const log = [];

    const C = parseMachine("C", {
        events: {
            SELF: { target: "self", actions: () => log.push("SELF") },
        },
        parallel: true,
        states: {
            E: {},
            D: {},
        },
    });

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                states: {
                    B: {
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        src: C,
                        events: {
                            TO_B: { target: "B", actions: () => log.push("TO_B") },
                        },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    machine.send("TO_C");
    machine.send("SELF");
    machine.send("TO_B");
    assert.deepEqual(log, ["SELF", "TO_B"]);
});

test.add("composed: auto actions defined inside and outside", () => {
    const log = [];

    const C = parseMachine("C", {
        parallel: true,
        always: {
            target: "self",
            guard: () => {
                log.push("GUARD_INSIDE");
                return false;
            },
            actions: [() => log.push("INSIDE")],
        },
        states: {
            E: {},
            D: {},
        },
    });

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "C",
                states: {
                    B: {
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        src: C,
                        always: { target: "B", actions: () => log.push("OUTSIDE") },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    machine.send("TO_B");
    assert.deepEqual(log, ["GUARD_INSIDE", "OUTSIDE"]);
});

test.add("composed: done actions defined inside and outside", async () => {
    const log = [];

    const C = parseMachine("C", {
        parallel: true,
        states: {
            E: {
                activity: () => Promise.resolve(),
            },
            D: { activity: () => Promise.resolve() },
        },
        done: {
            target: "self",
            guard: () => {
                log.push("GUARD_INSIDE");
                return false;
            },
            actions: [() => log.push("INSIDE")],
        },
    });

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "C",
                states: {
                    C: {
                        src: C,
                        done: { target: "B", actions: () => log.push("OUTSIDE") },
                    },
                    B: {},
                },
            },
        },
    });

    const machine = root;
    machine.start();
    await machine.wait("root.A.B");
    assert.deepEqual(log, ["GUARD_INSIDE", "OUTSIDE"]);
});

test.add("composed: error actions defined inside and outside", async () => {
    const log = [];

    const C = parseMachine("C", {
        parallel: true,
        states: {
            E: {
                activity: () => Promise.resolve(),
            },
            D: { activity: () => Promise.reject() },
        },
        error: {
            target: "self",
            guard: () => {
                log.push("GUARD_INSIDE");
                return false;
            },
            actions: [() => log.push("INSIDE")],
        },
    });

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "C",
                states: {
                    C: {
                        src: C,
                        error: { target: "B", actions: () => log.push("OUTSIDE") },
                    },
                    B: {},
                },
            },
        },
    });

    const machine = root;
    machine.start();
    await machine.wait("root.A.B");
    assert.deepEqual(log, ["GUARD_INSIDE", "OUTSIDE"]);
});
test.run();
