import { Test } from "@picoware/check";
import assert from "node:assert/strict";
import { parseMachine } from "../src/exports.js";

const test = Test.fromUrl(import.meta.url);

test.add("Done Node: Fail if event transition is defined", () => {
    assert.throws(() =>
        parseMachine("root", {
            initial: "A",
            parallel: true,
            states: {
                A: {},
                DONE: {
                    events: {
                        TO_A: { target: "A" },
                    },
                },
            },
        })
    );
});

test.add("Done Node: Fail if event history is defined", () => {
    assert.throws(() =>
        parseMachine("root", {
            initial: "A",
            parallel: true,
            states: {
                A: {},
                DONE: {
                    history: "whatever",
                },
            },
        })
    );
});

test.add("Done Node: Fail if always transition is defined", () => {
    assert.throws(() =>
        parseMachine("root", {
            initial: "A",
            parallel: true,
            states: {
                A: {},
                DONE: {
                    always: { target: "A" },
                },
            },
        })
    );
});

test.run();
