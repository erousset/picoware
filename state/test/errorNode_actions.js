import { Test } from "@picoware/check";
import assert from "node:assert/strict";
import { parseMachine } from "../src/exports.js";

const test = Test.fromUrl(import.meta.url);

test.add("composed: enter/exit actions defined inside and outside", () => {
    const log = [];

    const ERROR = parseMachine("ERROR", {
        enter: () => log.push("enter INSIDE"),
        exit: () => log.push("exit INSIDE"),
    });

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                error: { target: "C" },
                states: {
                    B: {
                        events: {
                            FAIL: { target: "ERROR" },
                        },
                    },
                    ERROR: {
                        src: ERROR,
                        enter: () => log.push("enter OUTSIDE"),
                        exit: () => log.push("exit OUTSIDE"),
                    },
                },
            },
            C: {},
        },
    });

    const machine = root;

    machine.start();
    machine.send("FAIL");
    assert.deepEqual(log, ["enter OUTSIDE", "enter INSIDE", "exit INSIDE", "exit OUTSIDE"]);
});

test.run();
