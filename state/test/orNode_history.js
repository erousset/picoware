import { Test } from "@picoware/check";
import assert from "node:assert/strict";
import { parseMachine } from "../src/exports.js";

const test = Test.fromUrl(import.meta.url);

test.add("history enabled", () => {
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                events: {
                    RE_ENTER: { target: "self" },
                },
                history: true,
                initial: "B",
                states: {
                    B: {
                        events: {
                            TO_C: {
                                target: "C",
                            },
                        },
                    },
                    C: {},
                },
            },
        },
    });
    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A.B"));
    machine.send("RE_ENTER");
    assert.ok(machine.matches("root.A.B"));
    machine.send("TO_C");
    assert.ok(machine.matches("root.A.C"));
    machine.send("RE_ENTER");
    assert.ok(machine.matches("root.A.C"));
});

test.add("history disabled", () => {
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                events: {
                    RE_ENTER: { target: "self" },
                },
                initial: "B",
                states: {
                    B: {
                        events: {
                            TO_C: {
                                target: "C",
                            },
                        },
                    },
                    C: {},
                },
            },
        },
    });
    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A.B"));
    machine.send("RE_ENTER");
    assert.ok(machine.matches("root.A.B"));
    machine.send("TO_C");
    assert.ok(machine.matches("root.A.C"));
    machine.send("RE_ENTER");
    assert.ok(machine.matches("root.A.B"));
});

test.add("multi level history enabled", () => {
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                events: {
                    RE_ENTER: { target: "self" },
                },
                history: true,
                initial: "B",
                states: {
                    B: {
                        events: {
                            TO_C: {
                                target: "C",
                            },
                        },
                        history: true,
                        initial: "D",
                        states: {
                            D: {
                                events: {
                                    TO_E: {
                                        target: "E",
                                    },
                                },
                            },
                            E: {},
                        },
                    },
                    C: {
                        events: {
                            TO_B: { target: "B" },
                        },
                    },
                },
            },
        },
    });
    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A.B.D"));
    machine.send("RE_ENTER");
    assert.ok(machine.matches("root.A.B.D"));
    machine.send("TO_E");
    assert.ok(machine.matches("root.A.B.E"));
    machine.send("RE_ENTER");
    assert.ok(machine.matches("root.A.B.E"));
    machine.send("TO_C");
    assert.ok(machine.matches("root.A.C"));
    machine.send("TO_B");
    assert.ok(machine.matches("root.A.B.E"));
});

test.add("multi level history disabled", () => {
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                events: {
                    RE_ENTER: { target: "self" },
                },
                initial: "B",
                states: {
                    B: {
                        events: {
                            TO_C: {
                                target: "C",
                            },
                        },
                        initial: "D",
                        states: {
                            D: {
                                events: {
                                    TO_E: {
                                        target: "E",
                                    },
                                },
                            },
                            E: {},
                        },
                    },
                    C: {
                        events: {
                            TO_B: { target: "B" },
                        },
                    },
                },
            },
        },
    });
    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A.B.D"));
    machine.send("RE_ENTER");
    assert.ok(machine.matches("root.A.B.D"));
    machine.send("TO_E");
    assert.ok(machine.matches("root.A.B.E"));
    machine.send("RE_ENTER");
    assert.ok(machine.matches("root.A.B.D"));
    machine.send("TO_E");
    assert.ok(machine.matches("root.A.B.E"));
    machine.send("TO_C");
    assert.ok(machine.matches("root.A.C"));
    machine.send("TO_B");
    assert.ok(machine.matches("root.A.B.D"));
});

test.add("multi level history mixed", () => {
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                events: {
                    RE_ENTER: { target: "self" },
                },
                initial: "B",
                states: {
                    B: {
                        events: {
                            TO_C: {
                                target: "C",
                            },
                        },
                        history: true,
                        initial: "D",
                        states: {
                            D: {
                                events: {
                                    TO_E: {
                                        target: "E",
                                    },
                                },
                            },
                            E: {},
                        },
                    },
                    C: {
                        events: {
                            TO_B: { target: "B" },
                        },
                    },
                },
            },
        },
    });
    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A.B.D"));
    machine.send("TO_C");
    assert.ok(machine.matches("root.A.C"));
    machine.send("RE_ENTER");
    assert.ok(machine.matches("root.A.B.D"));
    machine.send("TO_E");
    assert.ok(machine.matches("root.A.B.E"));
    machine.send("RE_ENTER");
    assert.ok(machine.matches("root.A.B.E"));
    machine.send("TO_C");
    assert.ok(machine.matches("root.A.C"));
    machine.send("RE_ENTER");
    assert.ok(machine.matches("root.A.B.E"));
});

test.add("true conditional history", () => {
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                events: {
                    RE_ENTER: { target: "self" },
                },
                history: () => true,
                initial: "B",
                states: {
                    B: {
                        events: {
                            TO_C: {
                                target: "C",
                            },
                        },
                    },
                    C: {},
                },
            },
        },
    });
    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A.B"));
    machine.send("RE_ENTER");
    assert.ok(machine.matches("root.A.B"));
    machine.send("TO_C");
    assert.ok(machine.matches("root.A.C"));
    machine.send("RE_ENTER");
    assert.ok(machine.matches("root.A.C"));
});

test.add("false conditional history", () => {
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                events: {
                    RE_ENTER: { target: "self" },
                },
                history: () => false,
                initial: "B",
                states: {
                    B: {
                        events: {
                            TO_C: {
                                target: "C",
                            },
                        },
                    },
                    C: {},
                },
            },
        },
    });
    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A.B"));
    machine.send("RE_ENTER");
    assert.ok(machine.matches("root.A.B"));
    machine.send("TO_C");
    assert.ok(machine.matches("root.A.C"));
    machine.send("RE_ENTER");
    assert.ok(machine.matches("root.A.B"));
});

test.add("composed history inside", () => {

    const A = parseMachine("A", {
        events: {
            RE_ENTER: { target: "self" },
        },
        initial: "B",
        history: true,
        states: {
            B: {
                events: {
                    TO_C: {
                        target: "C",
                    },
                },
            },
            C: {},
        }
    })

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                src: A,
            },
        },
    });
    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A.B"));
    machine.send("RE_ENTER");
    assert.ok(machine.matches("root.A.B"));
    machine.send("TO_C");
    assert.ok(machine.matches("root.A.C"));
    machine.send("RE_ENTER");
    assert.ok(machine.matches("root.A.C"));
});

test.add("composed history outside", () => {

    const A = parseMachine("A", {
        events: {
            RE_ENTER: { target: "self" },
        },
        initial: "B",
        states: {
            B: {
                events: {
                    TO_C: {
                        target: "C",
                    },
                },
            },
            C: {},
        }
    })

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                src: A,
                history: true,
            },
        },
    });
    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A.B"));
    machine.send("RE_ENTER");
    assert.ok(machine.matches("root.A.B"));
    machine.send("TO_C");
    assert.ok(machine.matches("root.A.C"));
    machine.send("RE_ENTER");
    assert.ok(machine.matches("root.A.C"));
});

test.run();
