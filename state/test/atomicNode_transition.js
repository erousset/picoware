import { Test } from "@picoware/check";
import assert from "node:assert/strict";
import FakeTimers from "@sinonjs/fake-timers";
import { parseMachine } from "../src/exports.js";

const test = Test.fromUrl(import.meta.url);

test.add("event transition", () => {
    const root = parseMachine("ROOT", {
        initial: "STATE1",
        states: {
            STATE1: {
                events: {
                    NEXT: {
                        target: "STATE2",
                    },
                },
            },
            STATE2: {
                events: {
                    NEXT: {
                        target: "STATE3",
                    },
                },
            },
            STATE3: {},
        },
    });
    const machine = root;

    machine.start();
    assert.ok(machine.matches("ROOT.STATE1"));
    machine.send("NEXT");
    assert.ok(machine.matches("ROOT.STATE2"));
    machine.send("NEXT");
    assert.ok(machine.matches("ROOT.STATE3"));
});

test.add("event transition (wrong event)", () => {
    const root = parseMachine("ROOT", {
        initial: "STATE1",
        states: {
            STATE1: {
                events: {
                    NEXT: {
                        target: "STATE2",
                    },
                },
            },
            STATE2: {},
        },
    });
    const machine = root;

    machine.start();
    assert.ok(machine.matches("ROOT.STATE1"));
    machine.send("WRONG_EVENT");
    assert.ok(machine.matches("ROOT.STATE1"));
});

test.add("event transition (no event)", () => {
    const root = parseMachine("ROOT", {
        initial: "STATE1",
        states: {
            STATE1: {},
            STATE2: {},
        },
    });
    const machine = root;

    machine.start();
    assert.ok(machine.matches("ROOT.STATE1"));
    machine.send("WRONG_EVENT");
    assert.ok(machine.matches("ROOT.STATE1"));
});

test.add("event transition (multiple events)", () => {
    const root = parseMachine("ROOT", {
        initial: "STATE1",
        states: {
            STATE1: {
                events: {
                    NEXT: {
                        target: "STATE2",
                    },
                    SELF: {
                        target: "self",
                    },
                },
            },
            STATE2: {},
        },
    });
    const machine = root;

    machine.start();
    assert.ok(machine.matches("ROOT.STATE1"));
    machine.send("SELF");
    assert.ok(machine.matches("ROOT.STATE1"));
    machine.send("NEXT");
    assert.ok(machine.matches("ROOT.STATE2"));
});

test.add("auto transition", async () => {
    const root = parseMachine("root", {
        initial: "TRANSIENT",
        states: {
            TRANSIENT: {
                always: {
                    target: "END",
                },
            },
            END: {},
        },
    });
    const machine = root;
    machine.start();
    assert.ok(machine.matches("root.END"));
});

test.add("delayed event transition", async () => {
    let clock = FakeTimers.install();
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                events: {
                    NEXT: {
                        target: "B",
                        delay: 500,
                    },
                },
            },
            B: {},
        },
    });
    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A"));
    machine.send("NEXT");
    assert.ok(machine.matches("root.A"));
    clock.tick(500);
    await machine.wait("root.B");
    clock.uninstall();
});

test.add("delayed auto transition", async () => {
    let clock = FakeTimers.install();
    const root = parseMachine("root", {
        initial: "TRANSIENT",
        states: {
            TRANSIENT: {
                always: {
                    target: "DONE",
                    delay: 500,
                },
            },
            DONE: {},
        },
    });
    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.TRANSIENT"));
    clock.tick(500);
    await machine.wait("root.DONE");
    clock.uninstall();
});

test.add("multiple event transitions", () => {
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                events: {
                    NEXT: [
                        {
                            target: "B",
                        },
                        {
                            target: "C",
                        },
                    ],
                },
            },
            B: {},
            C: {},
        },
    });
    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A"));
    machine.send("NEXT");
    assert.ok(machine.matches("root.B"));
});

test.add("multiple auto transitions", () => {
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                always: [
                    {
                        target: "B",
                    },
                    {
                        target: "C",
                    },
                ],
            },
            B: {},
            C: {},
        },
    });
    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.B"));
});

test.add("guarded event transitions", () => {
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                events: {
                    NEXT: [
                        {
                            target: "B",
                            guard: () => false,
                        },
                        {
                            target: "C",
                            guard: () => true,
                        },
                    ],
                },
            },
            B: {},
            C: {},
        },
    });
    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A"));
    machine.send("NEXT");
    assert.ok(machine.matches("root.C"));
});

test.add("guarded auto transitions", () => {
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                always: [
                    {
                        target: "B",
                        guard: () => false,
                    },
                    {
                        target: "C",
                        guard: () => true,
                    },
                ],
            },
            B: {},
            C: {},
        },
    });
    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.C"));
});

test.add("composition: events transitions defined outside", () => {
    const C = parseMachine("C", {});

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                states: {
                    B: {
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        src: C,
                        events: {
                            TO_B: { target: "B" },
                        },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A.B"));
    machine.send("TO_C");
    assert.ok(machine.matches("root.A.C"));
    machine.send("TO_B");
    assert.ok(machine.matches("root.A.B"));
});

test.add("composition: auto transitions defined outside", () => {
    const C = parseMachine("C", {});

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "C",
                states: {
                    B: {
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        src: C,
                        always: { target: "B" },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A.B"));
    machine.send("TO_B");
    assert.ok(machine.matches("root.A.B"));
});

test.add("internal transition", () => {
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                events: {
                    NEXT: {
                        target: "internal",
                    },
                },
            },
            B: {},
        },
    });
    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A"));
    machine.send("NEXT");
    assert.ok(machine.matches("root.A"));
});

test.run();
