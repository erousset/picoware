import { Test } from "@picoware/check";
import assert from "node:assert/strict";
import { parseMachine } from "../src/exports.js";

const test = Test.fromUrl(import.meta.url);

test.add("initial is recursive", () => {
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                states: {
                    B: {
                        initial: "C",
                        states: {
                            C: {},
                        },
                    },
                },
            },
        },
    });
    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A"));
    assert.ok(machine.matches("root.A.B"));
    assert.ok(machine.matches("root.A.B.C"));
});

test.add("event transition is recursive", () => {
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                events: {
                    TO_B: { target: "B" },
                },
            },
            B: {
                initial: "C",
                states: {
                    C: {},
                },
            },
        },
    });
    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A"));
    machine.send("TO_B");
    assert.ok(machine.matches("root.B"));
    assert.ok(machine.matches("root.B.C"));
});

test.add("subnodetransition keeps parent unchanged", () => {
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                events: {
                    TO_B: { target: "B" },
                },
            },
            B: {
                events: {
                    TO_A: { target: "A" },
                },
                initial: "C",
                states: {
                    C: {
                        events: {
                            TO_D: { target: "D" },
                        },
                    },
                    D: {
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                },
            },
        },
    });
    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A"));
    machine.send("TO_B");
    assert.ok(machine.matches("root.B"));
    assert.ok(machine.matches("root.B.C"));
    machine.send("TO_D");
    assert.ok(machine.matches("root.B"));
    assert.ok(machine.matches("root.B.D"));
});

test.add("exiting is recursive", () => {
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                events: {
                    TO_B: { target: "B" },
                },
            },
            B: {
                events: {
                    TO_A: { target: "A" },
                },
                initial: "C",
                states: {
                    C: {},
                },
            },
        },
    });
    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A"));
    machine.send("TO_B");
    assert.ok(machine.matches("root.B"));
    assert.ok(machine.matches("root.B.C"));
    machine.send("TO_A");
    assert.ok(machine.matches("root.A"));
});

test.add("Most outer parent takes transition.", () => {
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                events: {
                    NEXT: { target: "D" },
                },
                initial: "B",
                states: {
                    B: {
                        events: {
                            NEXT: {
                                target: "C",
                            },
                        },
                    },
                    C: {},
                },
            },
            D: {},
        },
    });
    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A.B"));
    machine.send("NEXT");
    assert.ok(machine.matches("root.D"));
});

test.add("done transition", () => {
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                done: { target: "DONE" },
                error: { target: "ERROR" },
                states: {
                    B: {
                        events: {
                            FINISH: { target: "DONE" },
                            FAIL: { target: "ERROR" },
                        },
                    },
                    DONE: {},
                    ERROR: {},
                },
            },
            DONE: {},
            ERROR: {},
        },
    });
    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A.B"));
    machine.send("FINISH");
    assert.ok(machine.matches("root.DONE"));
});

test.add("errot transition", () => {
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                done: { target: "DONE" },
                error: { target: "ERROR" },
                states: {
                    B: {
                        events: {
                            FINISH: { target: "DONE" },
                            FAIL: { target: "ERROR" },
                        },
                    },
                    DONE: {},
                    ERROR: {},
                },
            },
            DONE: {},
            ERROR: {},
        },
    });
    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A.B"));
    machine.send("FAIL");
    assert.ok(machine.matches("root.ERROR"));
});

test.add("auto transition: ignore subnode", async () => {
    const root = parseMachine("root", {
        initial: "TRANSIENT",
        states: {
            TRANSIENT: {
                always: {
                    target: "END",
                },
                initial: "A",
                states: {
                    A: {},
                },
            },
            END: {},
        },
    });
    const machine = root;
    machine.start();
    assert.ok(machine.matches("root.END"));
});

test.add("composed: events transitions defined outside", () => {
    const C = parseMachine("C", {
        initial: "D",
        states: {
            E: {},
            D: {},
        },
    });

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                states: {
                    B: {
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        src: C,
                        events: {
                            TO_B: { target: "B" },
                        },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A.B"));
    machine.send("TO_C");
    assert.ok(machine.matches("root.A.C.D"));
    machine.send("TO_B");
    assert.ok(machine.matches("root.A.B"));
});

test.add("composed: auto transitions defined outside", () => {
    const C = parseMachine("C", {
        initial: "D",
        states: {
            E: {},
            D: {},
        },
    });

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "C",
                states: {
                    B: {
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        src: C,
                        always: { target: "B" },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A.B"));
    machine.send("TO_B");
    assert.ok(machine.matches("root.A.B"));
});

test.add("composed: done transitions defined outside", () => {
    const C = parseMachine("C", {
        initial: "D",
        states: {
            D: {
                events: {
                    FINISH: { target: "DONE" },
                },
            },
            DONE: {},
        },
    });

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                states: {
                    B: {
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        src: C,
                        done: { target: "B" },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A.B"));
    machine.send("TO_C");
    assert.ok(machine.matches("root.A.C.D"));
    machine.send("FINISH");
    assert.ok(machine.matches("root.A.B"));
});

test.add("composed: error transitions defined outside", () => {
    const C = parseMachine("C", {
        initial: "D",
        states: {
            D: {
                events: {
                    FAIL: { target: "ERROR" },
                },
            },
            ERROR: {},
        },
    });

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                states: {
                    B: {
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        src: C,
                        error: { target: "B" },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A.B"));
    machine.send("TO_C");
    assert.ok(machine.matches("root.A.C.D"));
    machine.send("FAIL");
    assert.ok(machine.matches("root.A.B"));
});

test.add("composed: events transitions defined outside", () => {
    const C = parseMachine("C", {
        parallel: true,
        states: {
            E: {},
            D: {},
        },
    });

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                states: {
                    B: {
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        src: C,
                        events: {
                            TO_B: { target: "B" },
                        },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A.B"));
    machine.send("TO_C");
    assert.ok(machine.matches("root.A.C.D"));
    assert.ok(machine.matches("root.A.C.E"));
    machine.send("TO_B");
    assert.ok(machine.matches("root.A.B"));
});

test.add("composed: auto transitions defined outside", () => {
    const C = parseMachine("C", {
        parallel: true,
        states: {
            E: {},
            D: {},
        },
    });

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "C",
                states: {
                    B: {
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        src: C,
                        always: { target: "B" },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A.B"));
    machine.send("TO_B");
    assert.ok(machine.matches("root.A.B"));
});

test.add("internal transition doesn't affect subnodes", () => {
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                history: false,
                events: {
                    SELF: {
                        target: "internal",
                    },
                },
                initial: "B",
                states: {
                    B: {
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {},
                },
            },
        },
    });
    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A.B"));
    machine.send("TO_C");
    assert.ok(machine.matches("root.A.C"));
    machine.send("SELF");
    assert.ok(machine.matches("root.A.C"));
});

test.add("multiple done levels", () => {
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                states: {
                    B: {
                        initial: "C",
                        done: { target: "DONE" },
                        states: {
                            C: {
                                events: {
                                    FINISH: { target: "DONE" },
                                },
                            },
                            DONE: {},
                        },
                    },
                    DONE: {},
                },
            },
            DONE: {},
        },
    });
    const machine = root;

    const log = [];
    machine.status.onChanged((status) => log.push(status.state));
    machine.start();
    machine.send("FINISH");
    assert.deepEqual(log, [["root"], ["root.A"], ["root.A.B"], ["root.A.B.C"], ["root.A.B.DONE"], ["root.A.DONE"]]);
});

test.run();
