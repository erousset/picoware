import { Test } from "@picoware/check";
import assert from "node:assert/strict";
import { parseMachine } from "../src/exports.js";

const test = Test.fromUrl(import.meta.url);

test.add("AndNode: Fail if subnode source is not a node", () => {
    assert.throws(() =>
        parseMachine("root", {
            parallel: true,
            states: {
                A: {
                    src: "A",
                },
                B: {},
            },
        })
    );
});

test.add("And Node: Fail if subnode source doesn't match subnode name", () => {
    const A = parseMachine("D", {});
    assert.throws(() =>
        parseMachine("root", {
            parallel: true,
            states: {
                A: {
                    src: A,
                },
                B: {},
            },
        })
    );
});

test.add("And Node: Fail if both initial and parallel are defined", () => {
    assert.throws(() =>
        parseMachine("root", {
            initial: "A",
            parallel: true,
            states: {
                A: {},
                B: {},
            },
        })
    );
});

test.add("And Node: Fail if both history and parallel are defined", () => {
    assert.throws(() =>
        parseMachine("root", {
            history: true,
            parallel: true,
            states: {
                A: {},
                B: {},
            },
        })
    );
});

test.run();
