import { Test } from "@picoware/check";
import assert from "node:assert/strict";
import { parseMachine } from "../src/exports.js";

const test = Test.fromUrl(import.meta.url);

test.add("multiple done transitions", () => {
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                states: {
                    B: {
                        events: {
                            FINISH_C: { target: "DONE_C" },
                            FINISH_D: { target: "DONE_D" },
                        },
                    },
                    DONE_C: {
                        final: "done",
                    },
                    DONE_D: {
                        final: "done",
                    },
                },
                done: [
                    {
                        target: "C",
                        guard: (data) => data == "DONE_C",
                    },
                    {
                        target: "D",
                        guard: (data) => data == "DONE_D",
                    },
                ],
            },
            C: {
                events: {
                    TO_A: { target: "A" },
                },
            },
            D: {},
        },
    });

    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A.B"));
    machine.send("FINISH_C");
    assert.ok(machine.matches("root.C"));
    machine.send("TO_A");
    assert.ok(machine.matches("root.A.B"));
    machine.send("FINISH_D");
    assert.ok(machine.matches("root.D"));
});

test.add("multiple done transitions with default", () => {
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                states: {
                    B: {
                        events: {
                            FINISH_C: { target: "DONE" },
                            FINISH_D: { target: "DONE_D" },
                        },
                    },
                    DONE: {},
                    DONE_D: {
                        final: "done",
                    },
                },
                done: [
                    { target: "C", guard: (data) => data == "DONE" },
                    { target: "D", guard: (data) => data == "DONE_D" },
                ],
            },
            C: {
                events: {
                    TO_A: { target: "A" },
                },
            },
            D: {},
        },
    });

    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A.B"));
    machine.send("FINISH_C");
    assert.ok(machine.matches("root.C"));
    machine.send("TO_A");
    assert.ok(machine.matches("root.A.B"));
    machine.send("FINISH_D");
    assert.ok(machine.matches("root.D"));
});

test.run();
