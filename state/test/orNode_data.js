import { Test } from "@picoware/check";
import assert from "node:assert/strict";
import { parseMachine } from "../src/exports.js";

const test = Test.fromUrl(import.meta.url);

test.add("event data sending (single)", () => {
    const log = [];
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                states: {
                    B: {
                        events: {
                            NEXT: {
                                target: "C",
                                actions: (data) => log.push(data),
                            },
                        },
                    },
                    C: {},
                },
            },
        },
    });
    const machine = root;

    machine.start();
    assert.deepEqual(log, []);
    machine.send("NEXT", "data");
    assert.deepEqual(log, ["data"]);
});

test.add("event data sending (multiple)", () => {
    const log = [];

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                states: {
                    B: {
                        events: {
                            NEXT: {
                                target: "C",
                                actions: [
                                    (data) => log.push(data.data1),
                                    (data) => log.push(data.data2),
                                ],
                            },
                        },
                    },
                    C: {},
                },
            },
        },
    });
    const machine = root;

    machine.start();
    assert.deepEqual(log, []);
    machine.send("NEXT", { data1: "data1", data2: "data2" });
    assert.deepEqual(log, ["data1", "data2"]);
});

test.run();
