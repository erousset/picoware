import { Test } from "@picoware/check";
import assert from "node:assert/strict";
import { parseMachine } from "../src/exports.js";

const test = Test.fromUrl(import.meta.url);

test.add("event data (single)", () => {
    const log = [];

    const root = parseMachine("ROOT", {
        initial: "STATE1",
        states: {
            STATE1: {
                events: {
                    NEXT: {
                        target: "STATE2",
                        actions: (data) => log.push(data),
                    },
                },
            },
            STATE2: {},
        },
    });
    const machine = root;

    machine.start();
    assert.deepEqual(log, []);
    machine.send("NEXT", "data");
    assert.deepEqual(log, ["data"]);
});

test.add("event data sending (multiple)", () => {
    const log = [];

    const root = parseMachine("ROOT", {
        initial: "STATE1",
        states: {
            STATE1: {
                events: {
                    NEXT: {
                        target: "STATE2",
                        actions: [
                            (data) => log.push(data.data1),
                            (data) => log.push(data.data2)
                        ],
                    },
                },
            },
            STATE2: {},
        },
    });
    const context = { log: [] };
    const machine = root;

    machine.start();
    assert.deepEqual(log, []);
    machine.send("NEXT", { data1: "data1", data2: "data2" });
    assert.deepEqual(log, ["data1", "data2"]);
});

test.run();
