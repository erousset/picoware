import { Test } from "@picoware/check";
import assert from "node:assert/strict";
import { parseMachine } from "../src/exports.js";

const test = Test.fromUrl(import.meta.url);

test.add("multiple error transitions", () => {
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                states: {
                    B: {
                        events: {
                            FAIL_C: { target: "ERROR_C" },
                            FAIL_D: { target: "ERROR_D" },
                        },
                    },
                    ERROR_C: {
                        final: "error",
                    },
                    ERROR_D: {
                        final: "error",
                    },
                },
                error: [
                    { target: "C", guard: (_, data) => data == "ERROR_C" },
                    { target: "D", guard: (_, data) => data == "ERROR_D" },
                ],
            },
            C: {
                events: {
                    TO_A: { target: "A" },
                },
            },
            D: {},
        },
    });

    const machine = root;

    machine.start();
    machine.matches("root.A.B");
    machine.send("FAIL_C");
    machine.matches("root.C");
    machine.send("TO_A");
    machine.matches("root.A.B");
    machine.send("FAIL_D");
    machine.matches("root.D");
});

test.add("multiple done transitions with default", () => {
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                states: {
                    B: {
                        events: {
                            FAIL_C: { target: "ERROR" },
                            FAIL_D: { target: "ERROR_D" },
                        },
                    },
                    ERROR: {},
                    ERROR_D: {
                        final: "error",
                    },
                },
                error: [
                    { target: "C", guard: (_, data) => data == "ERROR" },
                    { target: "D", guard: (_, data) => data == "ERROR_D" },
                ],
            },
            C: {
                events: {
                    TO_A: { target: "A" },
                },
            },
            D: {},
        },
    });

    const machine = root;

    machine.start();
    machine.matches("root.A.B");
    machine.send("FAIL_C");
    machine.matches("root.C");
    machine.send("TO_A");
    machine.matches("root.A.B");
    machine.send("FAIL_D");
    machine.matches("root.D");
});

test.run();
