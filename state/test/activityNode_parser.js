import { Test } from "@picoware/check";
import assert from "node:assert/strict";
import { parseMachine } from "../src/exports.js";

const test = Test.fromUrl(import.meta.url);

test.add("Activity Node: Fail if subnodes are defined ", () => {
    assert.throws(() =>
        parseMachine("root", {
            activity: Promise.resolve(),
            states: {
                A: {},
                ERROR: {
                    always: { target: "A" },
                },
            },
        })
    );
});

test.add("Activity Node: Fail if parallel is defined ", () => {
    assert.throws(() =>
        parseMachine("root", {
            activity: Promise.resolve(),
            parallel: true,
        })
    );
});

test.add("Activity Node: Fail if initial is defined ", () => {
    assert.throws(() =>
        parseMachine("root", {
            activity: Promise.resolve(),
            initial: "whatever",
        })
    );
});

test.add("Activity Node: Fail if history is defined ", () => {
    assert.throws(() =>
        parseMachine("root", {
            activity: Promise.resolve(),
            history: true,
        })
    );
});

test.run();
