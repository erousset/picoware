import { Test } from "@picoware/check";
import assert from "node:assert/strict";
import { parseMachine } from "../src/exports.js";

const test = Test.fromUrl(import.meta.url);

test.add("enter/exit action (single)", () => {
    const log = [];

    const root = parseMachine("ROOT", {
        initial: "STATE1",
        states: {
            STATE1: {
                enter: () => log.push("enter"),
                exit: () => log.push("exit"),
                events: {
                    NEXT: {
                        target: "STATE2",
                    },
                },
            },
            STATE2: {},
        },
    });
    const machine = root;

    machine.start();
    assert.deepEqual(log, ["enter"]);
    machine.send("NEXT");
    assert.deepEqual(log, ["enter", "exit"]);
});

test.add("enter/exit action (multiple)", () => {
    const log = [];

    const root = parseMachine("ROOT", {
        initial: "STATE1",
        states: {
            STATE1: {
                enter: [() => log.push("enter"), () => log.push("enter2")],
                exit: [() => log.push("exit"), () => log.push("exit2")],
                events: {
                    NEXT: {
                        target: "STATE2",
                    },
                },
            },
            STATE2: {},
        },
    });
    const machine = root;

    machine.start();
    assert.deepEqual(log, ["enter", "enter2"]);
    machine.send("NEXT");
    assert.deepEqual(log, ["enter", "enter2", "exit", "exit2"]);
});

test.add("event transition (single action)", () => {
    const log = [];

    const root = parseMachine("ROOT", {
        initial: "STATE1",
        states: {
            STATE1: {
                events: {
                    NEXT: {
                        target: "STATE2",
                        actions: () => log.push("next"),
                    },
                },
            },
            STATE2: {},
        },
    });
    const machine = root;

    machine.start();
    assert.deepEqual(log, []);
    machine.send("NEXT");
    assert.deepEqual(log, ["next"]);
});

test.add("event transition (multiple actions)", () => {
    const log = [];

    const root = parseMachine("ROOT", {
        initial: "STATE1",
        states: {
            STATE1: {
                events: {
                    NEXT: {
                        target: "STATE2",
                        actions: [() => log.push("next"), () => log.push("next2")],
                    },
                },
            },
            STATE2: {},
        },
    });
    const machine = root;

    machine.start();
    assert.deepEqual(log, []);
    machine.send("NEXT");
    assert.deepEqual(log, ["next", "next2"]);
});

test.add("wrong event transition", () => {
    const log = [];

    const root = parseMachine("ROOT", {
        initial: "STATE1",
        states: {
            STATE1: {
                events: {
                    NEXT: {
                        target: "STATE2",
                        actions: () => log.push("next"),
                    },
                },
            },
            STATE2: {},
        },
    });
    const machine = root;

    machine.start();
    assert.deepEqual(log, []);
    machine.send("WRONG_EVENT");
    assert.deepEqual(log, []);
});

test.add("multiple event transitions", () => {
    const log = [];

    const root = parseMachine("ROOT", {
        initial: "STATE1",
        states: {
            STATE1: {
                events: {
                    NEXT: {
                        target: "STATE2",
                        actions: () => log.push("next"),
                    },
                    SELF: {
                        target: "self",
                        actions: () => log.push("self"),
                    },
                },
            },
            STATE2: {},
        },
    });
    const machine = root;

    machine.start();
    assert.deepEqual(log, []);
    machine.send("SELF");
    assert.deepEqual(log, ["self"]);
    machine.send("NEXT");
    assert.deepEqual(log, ["self", "next"]);
});

test.add("exit/event/enter action order", () => {
    const log = [];
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                enter: () => log.push("enterA"),
                exit: () => log.push("exitA"),
                events: {
                    TO_B: { target: "B", actions: () => log.push("transit A->B") },
                },
            },
            B: {
                enter: () => log.push("enterB"),
            },
        },
    });
    const machine = root;

    machine.start();
    assert.deepEqual(log, ["enterA"]);
    machine.send("TO_B");
    assert.deepEqual(log, ["enterA", "exitA", "transit A->B", "enterB"]);
});

test.add("auto transition (single action)", async () => {
    const log = [];
    const root = parseMachine("root", {
        initial: "TRANSIENT",
        states: {
            TRANSIENT: {
                always: {
                    target: "END",
                    actions: () => log.push("transient action"),
                },
            },
            END: {},
        },
    });
    const machine = root;
    machine.start();
    assert.deepEqual(log, ["transient action"]);
});

test.add("auto transition (multi actions)", async () => {
    const log = [];
    const root = parseMachine("root", {
        initial: "TRANSIENT",
        states: {
            TRANSIENT: {
                always: {
                    target: "END",
                    actions: [() => log.push("transient action"), () => log.push("transient action2")],
                },
            },
            END: {},
        },
    });
    const machine = root;
    machine.start();
    assert.deepEqual(log, ["transient action", "transient action2"]);
});

test.add("enter/auto/exit actions order", async () => {
    const log = [];
    const root = parseMachine("root", {
        initial: "TRANSIENT",
        states: {
            TRANSIENT: {
                enter: () => log.push("enterTRANSIENT"),
                exit: () => log.push("exitTRANSIENT"),
                always: {
                    target: "END",
                    actions: () => log.push("transient TRANSIENT->END"),
                },
            },
            END: {
                enter: () => log.push("enterEND"),
            },
        },
    });
    const machine = root;
    machine.start();
    assert.deepEqual(log, ["enterTRANSIENT", "exitTRANSIENT", "transient TRANSIENT->END", "enterEND"]);
});

test.add("composed: enter/exit actions defined inside and outside", () => {
    const log = [];

    const C = parseMachine("C", {
        enter: () => log.push("enter INSIDE"),
        exit: () => log.push("exit INSIDE"),
    });

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                states: {
                    B: {
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        src: C,
                        enter: () => log.push("enter OUTSIDE"),
                        exit: () => log.push("exit OUTSIDE"),
                        events: {
                            TO_B: { target: "B" },
                        },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    machine.send("TO_C");
    machine.send("TO_B");
    assert.deepEqual(log, ["enter OUTSIDE", "enter INSIDE", "exit INSIDE", "exit OUTSIDE"]);
});

test.add("composed: events actions defined inside and outside", () => {
    const log = [];

    const C = parseMachine("C", {
        events: {
            SELF: { target: "self", actions: [() => log.push("SELF")] },
        },
    });

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                states: {
                    B: {
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        src: C,
                        events: {
                            TO_B: { target: "B", actions: [() => log.push("TO_B")] },
                        },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    machine.send("TO_C");
    machine.send("SELF");
    machine.send("TO_B");
    assert.deepEqual(log, ["SELF", "TO_B"]);
});

test.add("composed: auto actions defined inside and outside", () => {
    const log = [];

    const C = parseMachine("C", {
        always: {
            target: "self",
            guard: () => {
                log.push("GUARD_INSIDE");
                return false;
            },
            actions: [() => log.push("INSIDE")],
        },
    });
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "C",
                states: {
                    B: {
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        src: C,
                        always: { target: "B", actions: [() => log.push("OUTSIDE")] },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    machine.send("TO_B");
    assert.deepEqual(log, ["GUARD_INSIDE", "OUTSIDE"]);
});

test.add("internal transition", () => {
    const log = [];

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                events: {
                    NEXT: {
                        target: "internal",
                        actions: () => log.push("INTERNAL"),
                    },
                },
            },
            B: {},
        },
    });
    const machine = root;

    machine.start();
    machine.send("NEXT");
    assert.deepEqual(log, ["INTERNAL"]);
});

test.add("context side effect", () => {
    const context = [];

    const root = parseMachine("root", {
        context: context,
        initial: "A",
        states: {
            A: {
                events: {
                    NEXT: {
                        target: "internal",
                        actions: () => context.push("INTERNAL"),
                    },
                },
            },
            B: {},
        },
    });
    const machine = root;

    machine.start();
    machine.send("NEXT");
    assert.deepEqual(machine.context, ["INTERNAL"]);
});

test.run();
