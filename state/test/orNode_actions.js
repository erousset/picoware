import { Test } from "@picoware/check";
import assert from "node:assert/strict";
import { parseMachine, raise } from "../src/exports.js";

const test = Test.fromUrl(import.meta.url);

test.add("enter is top down priotity", () => {
    const log = [];
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                enter: () => log.push("enterA"),
                initial: "B",
                states: {
                    B: {
                        enter: () => log.push("enterB"),
                        initial: "C",
                        states: {
                            C: {
                                enter: () => log.push("enterC"),
                            },
                        },
                    },
                },
            },
        },
    });
    const machine = root;

    machine.start();
    assert.deepEqual(log, ["enterA", "enterB", "enterC"]);
});

test.add("exit is bottom up priotity", () => {
    const log = [];
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                exit: () => log.push("exitA"),
                initial: "B",
                events: {
                    TO_D: {
                        target: "D",
                    },
                },
                states: {
                    B: {
                        exit: () => log.push("exitB"),
                        initial: "C",
                        states: {
                            C: {
                                exit: () => log.push("exitC"),
                            },
                        },
                    },
                },
            },
            D: {},
        },
    });
    const machine = root;

    machine.start();
    assert.deepEqual(log, []);
    machine.send("TO_D");
    assert.deepEqual(log, ["exitC", "exitB", "exitA"]);
});

test.add("exit/event/enter action order", () => {
    const log = [];
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                enter: () => log.push("enterA"),
                exit: () => log.push("exitA"),
                events: {
                    TO_B: { target: "B", actions: () => log.push("transit A->B") },
                },
            },
            B: {
                enter: () => log.push("enterB"),
                initial: "C",
                states: {
                    C: {
                        enter: () => log.push("enterC"),
                    },
                },
            },
        },
    });
    const machine = root;

    machine.start();
    assert.deepEqual(log, ["enterA"]);
    machine.send("TO_B");
    assert.deepEqual(log, ["enterA", "exitA", "transit A->B", "enterB", "enterC"]);
});

test.add("done actions (single)", () => {
    const log = [];
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                done: { target: "DONE", actions: () => log.push("doneB") },
                error: { target: "ERROR", actions: () => log.push("errorB") },
                states: {
                    B: {
                        events: {
                            FINISH: { target: "DONE" },
                            FAIL: { target: "ERROR" },
                        },
                    },
                    DONE: {},
                    ERROR: {},
                },
            },
            DONE: {},
            ERROR: {},
        },
    });
    const machine = root;

    machine.start();
    assert.deepEqual(log, []);
    machine.send("FINISH");
    assert.deepEqual(log, ["doneB"]);
});

test.add("done actions (multi)", () => {
    const log = [];
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                done: { target: "DONE", actions: [() => log.push("doneB"), () => log.push("doneB2")] },
                error: { target: "ERROR", actions: [() => log.push("errorB"), () => log.push("errorB2")] },
                states: {
                    B: {
                        events: {
                            FINISH: { target: "DONE" },
                            FAIL: { target: "ERROR" },
                        },
                    },
                    DONE: {},
                    ERROR: {},
                },
            },
            DONE: {},
            ERROR: {},
        },
    });
    const machine = root;

    machine.start();
    assert.deepEqual(log, []);
    machine.send("FINISH");
    assert.deepEqual(log, ["doneB", "doneB2"]);
});

test.add("error actions (single)", () => {
    const log = [];
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                done: { target: "DONE", actions: () => log.push("doneB") },
                error: { target: "ERROR", actions: () => log.push("errorB") },
                states: {
                    B: {
                        events: {
                            FINISH: { target: "DONE" },
                            FAIL: { target: "ERROR" },
                        },
                    },
                    DONE: {},
                    ERROR: {},
                },
            },
            DONE: {},
            ERROR: {},
        },
    });
    const machine = root;

    machine.start();
    assert.deepEqual(log, []);
    machine.send("FAIL");
    assert.deepEqual(log, ["errorB"]);
});

test.add("error actions (multi)", () => {
    const log = [];
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                done: { target: "DONE", actions: [() => log.push("doneB"), () => log.push("doneB2")] },
                error: { target: "ERROR", actions: [() => log.push("errorB"), () => log.push("errorB2")] },
                states: {
                    B: {
                        events: {
                            FINISH: { target: "DONE" },
                            FAIL: { target: "ERROR" },
                        },
                    },
                    DONE: {},
                    ERROR: {},
                },
            },
            DONE: {},
            ERROR: {},
        },
    });
    const machine = root;

    machine.start();
    assert.deepEqual(log, []);
    machine.send("FAIL");
    assert.deepEqual(log, ["errorB", "errorB2"]);
});

test.add("auto transition: ignore subnodes", async () => {
    const log = [];
    const root = parseMachine("root", {
        initial: "TRANSIENT",
        states: {
            TRANSIENT: {
                always: {
                    target: "END",
                    actions: () => log.push("transient TRANSIENT->END"),
                },
                initial: "A",
                states: {
                    A: {
                        always: {
                            target: "B",
                            actions: () => log.push("transient A->B"),
                        },
                    },
                    B: {},
                },
            },
            END: {},
        },
    });
    const machine = root;
    machine.start();
    assert.deepEqual(log, ["transient TRANSIENT->END"]);
});

test.add("raise event", async () => {
    const log = [];
    const root = parseMachine("root", {
        initial: "A",
        events: {
            RAISED: { target: "self", actions: () => log.push("RAISED") }
        },
        states: {
            A: {
                events: {
                    TO_B: { target: "B" },
                },
            },
            B: {
                events: {
                    RAISE: {
                        target: "self",
                        actions: raise("RAISED")
                    }
                }
            },
        },
    });
    const machine = root;

    machine.start();
    machine.send("TO_B");
    machine.send("RAISE");
    await machine.wait("root.A");
    assert.deepEqual(log, ["RAISED"]);
})

test.add("composed: raise event", async () => {
    const log = [];

    const B = parseMachine("B", {
        events: {
            RAISE: {
                target: "self",
                actions: raise("RAISED")
            }
        }
    })

    const root = parseMachine("root", {
        initial: "A",
        events: {
            RAISED: { target: "self", actions: () => log.push("RAISED") }
        },
        states: {
            A: {
                events: {
                    TO_B: { target: "B" },
                },
            },
            B: {
                src: B,
            },
        },
    });
    const machine = root;

    machine.start();
    machine.send("TO_B");
    machine.send("RAISE");
    await machine.wait("root.A");
    assert.deepEqual(log, ["RAISED"]);
})


test.add("composed: enter/exit actions defined inside and outside", () => {
    const log = [];

    const C = parseMachine("C", {
        enter: () => log.push("enter INSIDE"),
        exit: () => log.push("exit INSIDE"),
        initial: "D",
        states: {
            E: {},
            D: {},
        },
    });

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                states: {
                    B: {
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        src: C,
                        enter: () => log.push("enter OUTSIDE"),
                        exit: () => log.push("exit OUTSIDE"),
                        events: {
                            TO_B: { target: "B" },
                        },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    machine.send("TO_C");
    machine.send("TO_B");
    assert.deepEqual(log, ["enter OUTSIDE", "enter INSIDE", "exit INSIDE", "exit OUTSIDE"]);
});

test.add("composed: events actions defined inside and outside", () => {
    const log = [];

    const C = parseMachine("C", {
        initial: "D",
        events: {
            SELF: { target: "self", actions: () => log.push("SELF") },
        },
        states: {
            E: {},
            D: {},
        },
    });

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                states: {
                    B: {
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        src: C,
                        events: {
                            TO_B: { target: "B", actions: () => log.push("TO_B") },
                        },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    machine.send("TO_C");
    machine.send("SELF");
    machine.send("TO_B");
    assert.deepEqual(log, ["SELF", "TO_B"]);
});

test.add("composed: auto actions defined inside and outside", () => {
    const log = [];

    const C = parseMachine("C", {
        always: {
            target: "self",
            guard: () => {
                log.push("GUARD_INSIDE");
                return false;
            },
            actions: [() => log.push("INSIDE")],
        },
        initial: "D",
        states: {
            E: {},
            D: {},
        },
    });

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "C",
                states: {
                    B: {
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        src: C,
                        always: { target: "B", actions: () => log.push("OUTSIDE") },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    machine.send("TO_B");
    assert.deepEqual(log, ["GUARD_INSIDE", "OUTSIDE"]);
});

test.add("composed: done actions defined inside and outside", () => {
    const log = [];

    const C = parseMachine("C", {
        initial: "D",
        done: {
            target: "self",
            guard: () => {
                log.push("GUARD_INSIDE");
                return false;
            },
            actions: [() => log.push("INSIDE")],
        },
        states: {
            D: {
                events: {
                    FINISH: { target: "DONE" },
                },
            },
            DONE: {},
        },
    });

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                states: {
                    B: {
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        src: C,
                        done: { target: "B", actions: [() => log.push("OUTSIDE")] },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A.B"));
    machine.send("TO_C");
    machine.send("FINISH");
    assert.deepEqual(log, ["GUARD_INSIDE", "OUTSIDE"]);
});

test.add("composed: error actions defined inside and outside", () => {
    const log = [];

    const C = parseMachine("C", {
        initial: "D",
        error: {
            target: "self",
            guard: () => {
                log.push("GUARD_INSIDE");
                return false;
            },
            actions: [() => log.push("INSIDE")],
        },
        states: {
            D: {
                events: {
                    FAIL: { target: "ERROR" },
                },
            },
            ERROR: {},
        },
    });

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                states: {
                    B: {
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        src: C,
                        error: { target: "B", actions: [() => log.push("OUTSIDE")] },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    machine.send("TO_C");
    machine.send("FAIL");
    assert.deepEqual(log, ["GUARD_INSIDE", "OUTSIDE"]);
});


test.run();
