import { Test } from "@picoware/check";
import assert from "node:assert/strict";
import FakeTimers from "@sinonjs/fake-timers";
import { parseMachine } from "../src/exports.js";

const test = Test.fromUrl(import.meta.url);

test.add("activity resolves", async () => {
    const root = parseMachine("root", {
        initial: "PENDING",
        states: {
            PENDING: {
                activity: () => Promise.resolve(),
                done: {
                    target: "FULFILLED",
                },
                error: {
                    target: "REJECTED",
                },
            },
            FULFILLED: {},
            REJECTED: {},
        },
    });
    const machine = root;

    machine.start();
    await machine.wait("root.FULFILLED");
});

test.add("activity rejects", async () => {
    const root = parseMachine("root", {
        initial: "PENDING",
        states: {
            PENDING: {
                activity: () => Promise.reject(),
                done: {
                    target: "FULFILLED",
                },
                error: {
                    target: "REJECTED",
                },
            },
            FULFILLED: {},
            REJECTED: {},
        },
    });
    const machine = root;

    machine.start();
    await machine.wait("root.REJECTED", 1000);
});

test.add("activity: event transition", async () => {
    const root = parseMachine("root", {
        initial: "PENDING",
        states: {
            PENDING: {
                activity: () => Promise.resolve(),
                events: {
                    CANCEL: { target: "CANCELLED" },
                },
                done: {
                    target: "FULFILLED",
                },
                error: {
                    target: "REJECTED",
                },
            },
            FULFILLED: {},
            REJECTED: {},
            CANCELLED: {},
        },
    });
    const machine = root;

    machine.start();
    machine.send("CANCEL");
    await machine.wait("root.CANCELLED");
});

test.add("activity: auto transition", async () => {
    const root = parseMachine("root", {
        initial: "PENDING",
        states: {
            PENDING: {
                activity: () => Promise.resolve(),
                always: { target: "CANCELLED" },
                done: {
                    target: "FULFILLED",
                },
                error: {
                    target: "REJECTED",
                },
            },
            FULFILLED: {},
            REJECTED: {},
            CANCELLED: {},
        },
    });
    const machine = root;

    machine.start();
    machine.send("CANCEL");
    await machine.wait("root.CANCELLED");
});

test.add("multiple done transitions", async () => {
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                done: [
                    {
                        target: "B",
                    },
                    {
                        target: "C",
                    },
                ],
                error: [
                    {
                        target: "D",
                    },
                    {
                        target: "E",
                    },
                ],
                activity: () => Promise.resolve(),
            },
            B: {},
            C: {},
            D: {},
            E: {},
        },
    });
    const machine = root;

    machine.start();
    await machine.wait("root.B");
});

test.add("multiple error transitions", async () => {
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                done: [
                    {
                        target: "B",
                    },
                    {
                        target: "C",
                    },
                ],
                error: [
                    {
                        target: "D",
                    },
                    {
                        target: "E",
                    },
                ],
                activity: () => Promise.reject(),
            },
            B: {},
            C: {},
            D: {},
            E: {},
        },
    });
    const machine = root;

    machine.start();
    await machine.wait("root.D");
});

test.add("guarded done transitions", async () => {
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                done: [
                    {
                        target: "B",
                        guard: () => false,
                    },
                    {
                        target: "C",
                        guard: () => true,
                    },
                ],
                error: [
                    {
                        target: "D",
                        guard: () => false,
                    },
                    {
                        target: "E",
                        guard: () => true,
                    },
                ],
                activity: () => Promise.resolve(),
            },
            B: {},
            C: {},
            D: {},
            E: {},
        },
    });
    const machine = root;

    machine.start();
    await machine.wait("root.C");
});

test.add("guarded error transitions", async () => {
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                done: [
                    {
                        target: "B",
                        guard: () => false,
                    },
                    {
                        target: "C",
                        guard: () => true,
                    },
                ],
                error: [
                    {
                        target: "D",
                        guard: () => false,
                    },
                    {
                        target: "E",
                        guard: () => true,
                    },
                ],
                activity: () => Promise.reject(),
            },
            B: {},
            C: {},
            D: {},
            E: {},
        },
    });
    const machine = root;

    machine.start();
    await machine.wait("root.E");
});

test.add("delayed done transition", async () => {
    const clock = FakeTimers.install();

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                activity: () => Promise.resolve(),
                done: {
                    target: "B",
                    delay: 500,
                },
                error: {
                    target: "C",
                    delay: 500,
                },
            },
            B: {},
            C: {},
        },
    });
    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A"));
    // tick only after promise has resolved
    setTimeout(async () => {
        clock.tick(600);
        await machine.wait("root.B");
    }, 0);
    clock.uninstall();
});

test.add("delayed error", async () => {
    const clock = FakeTimers.install();

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                activity: () => Promise.reject(),
                done: {
                    target: "B",
                    delay: 500,
                },
                error: {
                    target: "C",
                    delay: 500,
                },
            },
            B: {},
            C: {},
        },
    });
    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A"));
    // tick only after promise has resolved
    setTimeout(async () => {
        clock.tick(600);
        await machine.wait("root.C");
    }, 0);
    clock.uninstall();
});

test.add("composed: done transitions defined outside", async () => {
    const C = parseMachine("C", {
        activity: () => Promise.resolve(),
    });

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                states: {
                    B: {
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        src: C,
                        done: { target: "B" },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A.B"));
    machine.send("TO_C");
    await machine.wait("root.A.B");
});

test.add("composed: error transitions defined outside", async () => {
    const C = parseMachine("C", {
        activity: () => Promise.reject(),
    });

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                states: {
                    B: {
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        src: C,
                        error: { target: "B" },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A.B"));
    machine.send("TO_C");
    await machine.wait("root.A.B");
});

test.add("composed: event transitions defined outside", () => {
    const C = parseMachine("C", {
        activity: () => new Promise(() => { }),
    });

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                states: {
                    B: {
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        src: C,
                        events: {
                            CANCEL: { target: "B" },
                        },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    assert.ok(machine.matches("root.A.B"));
    machine.send("TO_C");
    assert.ok(machine.matches("root.A.C"));
    machine.send("CANCEL");
    assert.ok(machine.matches("root.A.B"));
});

test.add("composed: auto transitions defined outside", async () => {
    const A = parseMachine("A", {
        activity: () => new Promise(() => { }),
    });

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                src: A,
                always: { target: "B" },
            },
            B: {},
        },
    });

    const machine = root;

    machine.start();
    await machine.wait("root.B");
});

test.run();
