import { Test } from "@picoware/check";
import assert from "node:assert/strict";
import { parseMachine } from "../src/exports.js";

const test = Test.fromUrl(import.meta.url);

test.add("done is raised", () => {
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                done: { target: "DONE" },
                error: { target: "ERROR" },
                states: {
                    B: {
                        events: {
                            FINISH: { target: "DONE" },
                            FAIL: { target: "ERROR" },
                        },
                    },
                    DONE: {},
                    ERROR: {},
                },
            },
            DONE: {},
            ERROR: {},
        },
    });
    const machine = root;
    let done = false;
    machine.onDone(() => (done = true));
    machine.start();
    assert.equal(done, false);
    machine.send("FINISH");
    assert.equal(done, true);
});

test.add("error is raised", () => {
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                done: { target: "DONE" },
                error: { target: "ERROR" },
                states: {
                    B: {
                        events: {
                            FINISH: { target: "DONE" },
                            FAIL: { target: "ERROR" },
                        },
                    },
                    DONE: {},
                    ERROR: {},
                },
            },
            DONE: {},
            ERROR: {},
        },
    });
    const machine = root;
    let error = false;
    machine.onError(() => (error = true));
    machine.start();
    assert.equal(error, false);
    machine.send("FAIL");
    assert.equal(error, true);
});

test.add("changed is raised", () => {
    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                events: {
                    TO_D: { target: "D" },
                },
                initial: "B",
                states: {
                    B: {
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {},
                },
            },
            D: {},
        },
    });
    const machine = root;
    let changeCount = 0;
    machine.status.onChanged(() => changeCount++);
    machine.start();
    assert.equal(changeCount, 3);
    machine.send("TO_C");
    assert.equal(changeCount, 4);
    machine.send("TO_D");
    assert.equal(changeCount, 5);
});
test.run();
