import { Test } from "@picoware/check";
import assert from "node:assert/strict";
import { parseMachine } from "../src/exports.js";

const test = Test.fromUrl(import.meta.url);

test.add("composition: enter/exit actions defined inside and outside", () => {
    const log = [];

    const DONE = parseMachine("DONE", {
        enter: () => log.push("enter INSIDE"),
        exit: () => log.push("exit INSIDE"),
    });

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                done: { target: "C" },
                states: {
                    B: {
                        events: {
                            FINISH: { target: "DONE" },
                        },
                    },
                    DONE: {
                        src: DONE,
                        enter: () => log.push("enter OUTSIDE"),
                        exit: () => log.push("exit OUTSIDE"),
                    },
                },
            },
            C: {},
        },
    });

    const machine = root;

    machine.start();
    machine.send("FINISH");
    assert.deepEqual(log, ["enter OUTSIDE", "enter INSIDE", "exit INSIDE", "exit OUTSIDE"]);
});

test.run();
