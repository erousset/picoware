import { Test } from "@picoware/check";
import assert from "node:assert/strict";
import { parseMachine } from "../src/exports.js";

const test = Test.fromUrl(import.meta.url);

test.add("Or Node: Wrong transition target", () => {
    assert.throws(() =>
        parseMachine("root", {
            initial: "A",
            states: {
                A: { events: { TO_C: { target: "C" } } },
                B: {},
            },
        })
    );
});

test.add("Or Node: Wrong initial state", () => {
    assert.throws(() =>
        parseMachine("root", {
            initial: "C",
            states: {
                A: {},
                B: {},
            },
        })
    );
});

test.add("Or Node: Missing initial state", () => {
    assert.throws(() =>
        parseMachine("root", {
            states: {
                A: {},
                B: {},
            },
        })
    );
});

test.add("Or Node: Fail if subnode source is not a node", () => {
    assert.throws(() =>
        parseMachine("root", {
            initial: "C",
            states: {
                A: {
                    src: "A",
                },
                B: {},
            },
        })
    );
});

test.add("Or Node: Fail if subnode source doesn't match subnode name", () => {
    const A = parseMachine("D", {});
    assert.throws(() =>
        parseMachine("root", {
            initial: "C",
            states: {
                A: {
                    src: A,
                },
                B: {},
            },
        })
    );
});

test.run();
