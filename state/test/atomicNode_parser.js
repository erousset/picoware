import { Test } from "@picoware/check";
import assert from "node:assert/strict";
import { parseMachine } from "../src/exports.js";

const test = Test.fromUrl(import.meta.url);

test.add("atomic Node: fail if history is defined", () => {
    assert.throws(() =>
        parseMachine("root", {
            history: true,
        })
    );
});

test.add("atomic Node: fail if initial is defined", () => {
    assert.throws(() =>
        parseMachine("root", {
            initial: "whatever",
        })
    );
});

test.add("atomic Node: fail if parallel is defined", () => {
    assert.throws(() =>
        parseMachine("root", {
            parallel: true,
        })
    );
});

test.add("atomic Node: fail if done is defined", () => {
    assert.throws(() =>
        parseMachine("root", {
            done: "whatever",
        })
    );
});

test.add("atomic Node: fail if error is defined", () => {
    assert.throws(() =>
        parseMachine("root", {
            error: "whatever",
        })
    );
});

test.run();
