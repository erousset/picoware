import { Test } from "@picoware/check";
import assert from "node:assert/strict";
import { parseMachine } from "../src/exports.js";

const test = Test.fromUrl(import.meta.url);

function activity(shouldResolve) {
    return new Promise((resolve, reject) => {
        if (shouldResolve) {
            resolve();
        } else {
            reject();
        }
    });
}

test.add("transition actions order on resolve (single)", async () => {
    const log = [];
    const root = parseMachine("root", {
        initial: "PENDING",
        states: {
            PENDING: {
                activity: () => activity(true),
                done: {
                    target: "FULFILLED",
                    actions: () => log.push("done PENDING"),
                },
                error: {
                    target: "REJECTED",
                    actions: () => log.push("error PENDING"),
                },
            },
            FULFILLED: {},
            REJECTED: {},
        },
    });
    const machine = root;

    machine.start();
    await machine.wait("root.FULFILLED");
    assert.deepEqual(log, ["done PENDING"]);
});

test.add("transition actions order on resolve (multi)", async () => {
    const log = [];
    const root = parseMachine("root", {
        initial: "PENDING",
        states: {
            PENDING: {
                activity: () => activity(true),
                done: {
                    target: "FULFILLED",
                    actions: [() => log.push("done PENDING"), () => log.push("done PENDING2")],
                },
                error: {
                    target: "REJECTED",
                    actions: [() => log.push("error PENDING"), () => log.push("error PENDING2")],
                },
            },
            FULFILLED: {},
            REJECTED: {},
        },
    });
    const machine = root;

    machine.start();
    await machine.wait("root.FULFILLED");
    assert.deepEqual(log, ["done PENDING", "done PENDING2"]);
});

test.add("transition actions order on reject (single)", async () => {
    const log = [];
    const root = parseMachine("root", {
        initial: "PENDING",
        states: {
            PENDING: {
                activity: () => activity(false),
                done: {
                    target: "FULFILLED",
                    actions: () => log.push("done PENDING"),
                },
                error: {
                    target: "REJECTED",
                    actions: () => log.push("error PENDING"),
                },
            },
            FULFILLED: {},
            REJECTED: {},
        },
    });
    const machine = root;

    machine.start();
    await machine.wait("root.REJECTED");
    assert.deepEqual(log, ["error PENDING"]);
});

test.add("transition actions order on reject (multi)", async () => {
    const log = [];
    const root = parseMachine("root", {
        initial: "PENDING",
        states: {
            PENDING: {
                activity: () => activity(false),
                done: {
                    target: "FULFILLED",
                    actions: [() => log.push("done PENDING"), () => log.push("done PENDING2")],
                },
                error: {
                    target: "REJECTED",
                    actions: [() => log.push("error PENDING"), () => log.push("error PENDING2")],
                },
            },
            FULFILLED: {},
            REJECTED: {},
        },
    });
    const machine = root;

    machine.start();
    await machine.wait("root.REJECTED");
    assert.deepEqual(log, ["error PENDING", "error PENDING2"]);
});

test.add("enter/exit/transition actions order on resolve", async () => {
    const log = [];
    const root = parseMachine("root", {
        initial: "PENDING",
        states: {
            PENDING: {
                enter: () => log.push("enterPENDING"),
                exit: () => log.push("exitPENDING"),
                activity: () => activity(true),
                done: {
                    target: "FULFILLED",
                    actions: () => log.push("done PENDING"),
                },
                error: {
                    target: "REJECTED",
                    actions: () => log.push("error PENDING"),
                },
            },
            FULFILLED: {
                enter: () => log.push("enterFULFILLED"),
            },
            REJECTED: {
                enter: () => log.push("enterREJECTED"),
            },
        },
    });
    const machine = root;

    machine.start();
    await machine.wait("root.FULFILLED");
    assert.deepEqual(log, ["enterPENDING", "exitPENDING", "done PENDING", "enterFULFILLED"]);
});

test.add("enter/exit/transition actions order on reject", async () => {
    const log = [];
    const root = parseMachine("root", {
        initial: "PENDING",
        states: {
            PENDING: {
                enter: () => log.push("enterPENDING"),
                exit: () => log.push("exitPENDING"),
                activity: () => activity(false),
                done: {
                    target: "FULFILLED",
                    actions: () => log.push("done PENDING"),
                },
                error: {
                    target: "REJECTED",
                    actions: () => log.push("error PENDING"),
                },
            },
            FULFILLED: {
                enter: () => log.push("enterFULFILLED"),
            },
            REJECTED: {
                enter: () => log.push("enterREJECTED"),
            },
        },
    });
    const machine = root;

    machine.start();
    await machine.wait("root.REJECTED");
    assert.deepEqual(log, ["enterPENDING", "exitPENDING", "error PENDING", "enterREJECTED"]);
});

test.add("activity: event action when resolves", async () => {
    const log = [];
    const root = parseMachine("root", {
        initial: "PENDING",
        states: {
            PENDING: {
                activity: () => Promise.resolve(),
                events: {
                    CANCEL: { target: "CANCELLED", actions: () => log.push("CANCEL") },
                },
                done: {
                    target: "FULFILLED",
                    actions: () => log.push("done"),
                },
                error: {
                    target: "REJECTED",
                    actions: () => log.push("error"),
                },
            },
            FULFILLED: {},
            REJECTED: {},
            CANCELLED: {},
        },
    });
    const machine = root;

    machine.start();
    machine.send("CANCEL");
    await machine.wait("root.CANCELLED");
    assert.deepEqual(log, ["CANCEL"]);
});

test.add("activity: event action when rejects", async () => {
    const log = [];
    const root = parseMachine("root", {
        initial: "PENDING",
        states: {
            PENDING: {
                activity: () => Promise.reject(),
                events: {
                    CANCEL: { target: "CANCELLED", actions: () => log.push("CANCEL") },
                },
                done: {
                    target: "FULFILLED",
                    actions: () => log.push("done"),
                },
                error: {
                    target: "REJECTED",
                    actions: () => log.push("error"),
                },
            },
            FULFILLED: {},
            REJECTED: {},
            CANCELLED: {},
        },
    });
    const machine = root;

    machine.start();
    machine.send("CANCEL");
    await machine.wait("root.CANCELLED");
    assert.deepEqual(log, ["CANCEL"]);
});

test.add("activity: auto action ", async () => {
    const log = [];
    const root = parseMachine("root", {
        initial: "PENDING",
        states: {
            PENDING: {
                activity: () => Promise.resolve(),
                always: { target: "CANCELLED" },
                done: {
                    target: "FULFILLED",
                    actions: () => log.push("done"),
                },
                error: {
                    target: "REJECTED",
                    actions: () => log.push("error"),
                },
            },
            FULFILLED: {},
            REJECTED: {},
            CANCELLED: {},
        },
    });
    const machine = root;

    machine.start();
    machine.send("CANCEL");
    await machine.wait("root.CANCELLED");
});

test.add("composed: enter/exit actions defined inside and outside", async () => {
    const log = [];

    const C = parseMachine("C", {
        activity: () => Promise.resolve(),
        enter: () => log.push("enter INSIDE"),
        exit: () => log.push("exit INSIDE"),
    });

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                states: {
                    B: {
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        src: C,
                        enter: () => log.push("enter OUTSIDE"),
                        exit: () => log.push("exit OUTSIDE"),
                        done: { target: "B" },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    machine.send("TO_C");
    await machine.wait("root.A.B");

    assert.deepEqual(log, ["enter OUTSIDE", "enter INSIDE", "exit INSIDE", "exit OUTSIDE"]);
});

test.add("composed: done actions defined inside and outside", async () => {
    const log = [];

    const C = parseMachine("C", {
        activity: () => Promise.resolve(),
        done: {
            target: "self",
            guard: () => {
                log.push("GUARD_INSIDE");
                return false;
            },
            actions: [() => log.push("INSIDE")],
        },
    });

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                states: {
                    B: {
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        src: C,
                        done: { target: "B", actions: [() => log.push("OUTSIDE")] },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    machine.send("TO_C");
    await machine.wait("root.A.B");
    assert.deepEqual(log, ["GUARD_INSIDE", "OUTSIDE"]);
});

test.add("composed: error actions defined inside and outside", async () => {
    const log = [];

    const C = parseMachine("C", {
        activity: () => Promise.reject(),
        error: {
            target: "self",
            guard: () => {
                log.push("GUARD_INSIDE");
                return false;
            },
            actions: [() => log.push("INSIDE")],
        },
    });

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                states: {
                    B: {
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        src: C,
                        error: { target: "B", actions: [() => log.push("OUTSIDE")] },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    machine.send("TO_C");
    await machine.wait("root.A.B");
    assert.deepEqual(log, ["GUARD_INSIDE", "OUTSIDE"]);
});

test.add("composed: event actions defined inside and outside", () => {
    const log = [];
    const C = parseMachine("C", {
        events: {
            SELF: { target: "self", actions: [() => log.push("SELF")] },
        },
        activity: () => new Promise(() => { }),
    });

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                initial: "B",
                states: {
                    B: {
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        src: C,
                        events: {
                            CANCEL: { target: "B", actions: [() => log.push("CANCEL")] },
                        },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    machine.send("TO_C");
    machine.send("SELF");
    machine.send("CANCEL");
    assert.deepEqual(log, ["SELF", "CANCEL"]);
});

test.add("composed: auto actions defined inside and outside", async () => {
    const log = [];
    const A = parseMachine("A", {
        activity: () => new Promise(() => { }),
        always: {
            target: "self",
            guard: () => {
                log.push("GUARD_INSIDE");
                return false;
            },
            actions: [() => log.push("INSIDE")],
        },
    });

    const root = parseMachine("root", {
        initial: "A",
        states: {
            A: {
                src: A,
                always: { target: "B", actions: [() => log.push("OUTSIDE")] },
            },
            B: {},
        },
    });

    const machine = root;

    machine.start();
    assert.deepEqual(log, ["GUARD_INSIDE", "OUTSIDE"]);
});

test.run();
