import { Test } from "@picoware/check";
import assert from "node:assert/strict";
import { parseMachine } from "../src/exports.js";

const test = Test.fromUrl(import.meta.url);

test.add("all states are entered", () => {
    const root = parseMachine("root", {
        parallel: true,
        states: {
            A: {},
            B: {},
            C: {},
        },
    });

    const machine = root;
    machine.start();
    assert.ok(machine.matches("root.A"));
    assert.ok(machine.matches("root.B"));
    assert.ok(machine.matches("root.C"));
});

test.add("done transition", async () => {
    const root = parseMachine("root", {
        initial: "BASE",
        states: {
            BASE: {
                parallel: true,
                done: {
                    target: "END",
                },
                error: {
                    target: "FAILED",
                },
                states: {
                    A: {
                        activity: () => Promise.resolve(),
                    },
                    B: {
                        activity: () => Promise.resolve(),
                    },
                    C: {},
                },
            },
            END: {},
            FAILED: {},
        },
    });

    const machine = root;

    machine.start();
    await machine.wait("root.END");
});

test.add("error transition", async () => {
    const root = parseMachine("root", {
        initial: "BASE",
        states: {
            BASE: {
                parallel: true,
                done: {
                    target: "END",
                },
                error: {
                    target: "FAILED",
                },
                states: {
                    A: {
                        activity: () => Promise.resolve(),
                    },
                    B: {
                        activity: () => Promise.reject(),
                    },
                    C: {},
                },
            },
            END: {},
            FAILED: {},
        },
    });

    const machine = root;

    machine.start();
    await machine.wait("root.FAILED");
});

test.add("root done transition", async () => {
    const root = parseMachine("root", {
        parallel: true,

        states: {
            A: {
                activity: () => Promise.resolve(),
            },
            B: {
                activity: () => Promise.resolve(),
            },
            C: {},
        },
    });

    const log = [];
    const machine = root;
    const promise = new Promise((resolve) => machine.onDone(() => { resolve() }));
    machine.start();
    await promise;
});

test.add("error done transition", async () => {
    const root = parseMachine("root", {
        parallel: true,

        states: {
            A: {
                activity: () => Promise.resolve(),
            },
            B: {
                activity: () => Promise.reject(),
            },
            C: {},
        },
    });

    const log = [];
    const machine = root;
    const promise = new Promise((resolve) => machine.onError(() => { resolve() }));
    machine.start();
    await promise;
});
test.run();
