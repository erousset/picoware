export class Signal {
    #slots = [];

    /**
     * Removes the given function from the listeners
     * @param {function} slot the function to be removed
     */
    #unsubscribe(slot) {
        this.#slots = this.#slots.filter((item) => item !== slot);
    }

    /**
     * Subscribes the given function to the signal and returns an unsubscribing function
     * @param {function} slot function called whenever the signal is raised.
     * @returns {function} unsubscribing function
     */
    on(slot) {
        this.#slots = [...this.#slots, slot];
        return () => this.#unsubscribe(slot);
    }

    /**
     * Raises a new value. All listeners will be called with this value as parameter.
     * @param {any} value Value passed to the listeners.
     */
    emit(value = null) {
        this.#slots.forEach((slot) => slot(value));
    }

    readonly() {
        let self = this;
        return {
            on: (slot) => {
                return self.on(slot);
            },
        };
    }
}
