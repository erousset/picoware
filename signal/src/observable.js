import { Signal } from "./signal.js";
/**
 *  @template T
 */
export class Observable {
    #value;
    #changed = new Signal();

    /**
     * @param {T} value Initial value
     */
    constructor(value) {
        this.#value = value;
    }

    set value(value) {
        if (value === this.#value) return;
        this.#value = value;
        this.#changed.emit(this.#value);
    }

    get value() {
        return this.#value;
    }

    get changed() {
        return this.#changed.readonly();
    }

    /**
     * Subscribes the given function to the observable and returns an unsubscribing function.
     * Calls the given function with the current value of the observable.
     * @param {(arg:T)=>void} slot function called whenever the observable changes
     * @returns {function} unsubscribing function
     */
    subscribe(slot) {
        slot(this.#value);
        return this.#changed.on(slot);
    }

    /**
     * Subscribes the given function to the observable and returns an unsubscribing function.
     * @param {(arg:T)=>void} slot function called whenever the observable changes
     * @returns {function} unsubscribing function
     */
    onChanged(slot) {
        return this.#changed.on(slot);
    }

    readonly() {
        let self = this;
        return {
            get changed() {
                return self.changed;
            },
            get value() {
                return self.value;
            },
            subscribe: (slot) => {
                return self.subscribe(slot);
            },
        };
    }
}
