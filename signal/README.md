# @picoware/signal

A tiny signal library. 

[Signal programming](https://en.wikipedia.org/wiki/Signal_programming) is a particular type of [event driven programming](https://en.wikipedia.org/wiki/Event-driven_programming). This library provides: 

* signals and slots (aka. events emitting and callbacks)
* observable values with auto-emitted signals on set

This library runs both in browser and on Node.js

## Installation
```  npm install @picoware/signal```

## Usage

### Signal

#### Basic signal

Declare a new signal and register a slot (aka. callback) to it. Then the slot will be called every time the signal is emitted :
```js
let data = 0;
let signal = new Signal();
signal.on(() => { data++ });

assert.equal(data, 0);
signal.emit();
assert.equal(data, 1);
signal.emit();
assert.equal(data, 2);
```

#### Signal with parameter

You can provide a parameter when emitting a signal. The parameter will be forwarded to the slot:

```js
let data = 0;
let signal = new Signal();
signal.on((value) => { data = value });

assert.equal(data, 0);
signal.emit(3);
assert.equal(data, 3);
```

#### Signal with multiple slots
You can provide multiple slots to a signal. They will be called by order of registration:

```js
let data = 0;
let signal = new Signal();
signal.on((value) => { data = value });
signal.on(() => { data *= 2 });

assert.equal(data, 0);
signal.emit(3);
assert.equal(data, 6);
```

#### Unsubscribe

The registering function returns an unregistering function which can be called at anytime to remove the connection:

```js
    let data = 0;
    let signal = new Signal();
    let unsubscribe = signal.on(() => { data++ });

    assert.equal(data, 0);
    signal.emit();
    assert.equal(data, 1);
    unsubscribe();
    signal.emit();
    assert.equal(data, 1); // data was not increased
```

### Observable
#### Basic observable

An `Observable` contains a `value` and a signal named `changed` that is automatically emitted each time `value` is set :

```js
let computed = 0;
let observable = new Observable(1);
observable.changed.on((value) => { computed = value * 2; });

assert.equal(computed, 0); // not 2, slot hasn't been called
observable.value = 2;
assert.equal(computed, 4);
observable.value = 5;
assert.equal(computed, 10);
```

Notice that the slot is NOT called with the current value upon registration. See `subscribe` below if you need such behaviour.

#### Registration shortcut

You can use the `onChanged` shortcut instead of `changed.on` to register your slots:

```js
let computed = 0;
let observable = new Observable(1);
observable.onChanged((value) => { computed = value * 2; });

assert.equal(computed, 0); // not 2, slot hasn't been called
observable.value = 2;
assert.equal(computed, 4);
observable.value = 5;
assert.equal(computed, 10);
```

#### Registration with execution
 
You can use the `subscribe` method if you want both to:
* register your slot to an observable
* execute your slot with the current value of the observable

```js
let computed = 0;
let observable = new Observable(1);
observable.subscribe((value) => { computed = value * 2; });

assert.equal(computed, 2); // here slot has been called
observable.value = 2;
assert.equal(computed, 4);
observable.value = 5;
assert.equal(computed, 10);
```

