/**
 *  @template T
 */
export class Observable<T> {
    /**
     * @param {T} value Initial value
     */
    constructor(value: T);
    set value(arg: T);
    get value(): T;
    get changed(): {
        on: (slot: any) => Function;
    };
    /**
     * Subscribes the given function to the observable and returns an unsubscribing function.
     * Calls the given function with the current value of the observable.
     * @param {(arg:T)=>void} slot function called whenever the observable changes
     * @returns {function} unsubscribing function
     */
    subscribe(slot: (arg: T) => void): Function;
    /**
     * Subscribes the given function to the observable and returns an unsubscribing function.
     * @param {(arg:T)=>void} slot function called whenever the observable changes
     * @returns {function} unsubscribing function
     */
    onChanged(slot: (arg: T) => void): Function;
    readonly(): {
        readonly changed: {
            on: (slot: any) => Function;
        };
        readonly value: T;
        subscribe: (slot: any) => Function;
    };
    #private;
}
//# sourceMappingURL=observable.d.ts.map