export class Signal {
    /**
     * Subscribes the given function to the signal and returns an unsubscribing function
     * @param {function} slot function called whenever the signal is raised.
     * @returns {function} unsubscribing function
     */
    on(slot: Function): Function;
    /**
     * Raises a new value. All listeners will be called with this value as parameter.
     * @param {any} value Value passed to the listeners.
     */
    emit(value?: any): void;
    readonly(): {
        on: (slot: any) => Function;
    };
    #private;
}
//# sourceMappingURL=signal.d.ts.map