import { Test } from "@picoware/check";
import assert from "node:assert/strict";
import { Observable } from "../src/exports.js";

const test = Test.fromUrl(import.meta.url);

test.add("slot is called each time value is modified", () => {

    let computed = 0;
    let observable = new Observable(1);
    observable.changed.on((value) => { computed = value * 2; });

    assert.equal(computed, 0);
    observable.value = 2;
    assert.equal(computed, 4);
    observable.value = 5;
    assert.equal(computed, 10);
});

test.add("onChanged shortcut works", () => {

    let computed = 0;
    let observable = new Observable(1);
    observable.onChanged((value) => { computed = value * 2; });

    assert.equal(computed, 0);
    observable.value = 2;
    assert.equal(computed, 4);
    observable.value = 5;
    assert.equal(computed, 10);
});

test.add("subscribe calls the slot with current value", () => {

    let computed = 0;
    let observable = new Observable(1);
    observable.subscribe((value) => { computed = value * 2; });

    assert.equal(computed, 2);
    observable.value = 2;
    assert.equal(computed, 4);
    observable.value = 5;
    assert.equal(computed, 10);
});



test.run();