import chalk from "chalk";
import * as path from "node:path";

export class Test {

    static tests = [];
    static runnable = false;
    static passed = 0;
    static failed = 0;
    static total = 0;

    static fromUrl(url) { return new Test(path.basename(url, ".js")); }

    static async run() {
        for (const test of Test.tests) { await test.run(); }

        if (Test.passed == Test.total)
            console.log(chalk.green(`Passed: ${Test.passed} / ${Test.total}`));
        else {
            console.log(chalk.red(`Failed: ${Test.failed} / ${Test.total}`));
        }
    }

    static formatError(e) {
        const msg = e.stack;
        if (!msg) return chalk.yellow(e);

        const delimiter = msg.indexOf("\n");
        const title = msg.slice(0, delimiter);
        const stacktrace = msg.slice(delimiter);

        return `${chalk.yellow(title)} ${chalk.gray(stacktrace)}\n`
    }

    // can log from another process
    static log = (str) => process.stdout.write(str);

    name;
    passed = 0;
    failed = 0;
    cases = [];
    message = "";

    constructor(name) {
        Test.tests.push(this);
        this.name = name;
    }

    add(name, fn) {
        this.cases.push({ name: name, fn: fn });
        Test.total++;
    }

    async run() {
        if (!Test.runnable) return;

        Test.log(chalk.cyan(this.name + " "));

        for (const test of this.cases) {

            let timeoutID = setTimeout(() => {
                this.message += chalk.red(`\n! ${test.name} `);
                this.message += Test.formatError("Test Timeout after 5000ms");
                Test.log(chalk.red("• "));
                this.failed++;
                Test.failed++;
            }, 5000);

            try {
                await test.fn();
                clearTimeout(timeoutID);
                Test.log(chalk.green("• "));
                this.passed++;
                Test.passed++;
            }
            catch (e) {
                clearTimeout(timeoutID);
                this.message += chalk.red(`\n! ${test.name} `);
                this.message += Test.formatError(e);
                Test.log(chalk.red("• "));
                this.failed++;
                Test.failed++;
            }
        }

        if (this.passed == this.cases.length) {
            Test.log(chalk.green(`✓\n`));
        } else {
            Test.log(chalk.red(`x\n`));
        }

        if (this.message) Test.log(this.message + "\n");
    }
}
