#!/usr/bin/env node

import { readdir, stat, rm, access } from "node:fs/promises";
import *as path from "node:path";
import chalk from "chalk";
import * as esbuild from 'esbuild'
import { Test } from "./index.js";

const BUILD = ".picocheck-build"
const ESBUILD_CONFIG = {
    bundle: false,
    outdir: BUILD,
}

const FOLDER = process.argv[2];

async function getFiles(folder) {
    try {
        await access(folder)
        return (await readdir(folder, { withFileTypes: true, recursive: true }))
            .filter(dirent => dirent.isFile())
            .map(dirent => path.join(dirent.path, dirent.name));
    } catch (e) {
        throw `ERROR - cant access folder ${folder}`
    }
}

async function build(folder) {
    const files = await getFiles(folder);
    await esbuild.build({
        entryPoints: files,
        ...ESBUILD_CONFIG
    })

}

async function run() {
    try {

        await build(FOLDER);

        const files = await getFiles(BUILD);

        Test.runnable = false;

        for (const file of files) {
            await import(`${process.cwd()}/${file}`);
        }

        Test.runnable = true;

        await Test.run();

    } catch (err) {
        console.error(chalk.red(err));
    }

    try {
        await rm(`./${BUILD}`, { recursive: true })
    }
    catch (e) {
        console.log("INFO - nothing to clean")
    }
}

run();
