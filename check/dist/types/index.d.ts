export declare class Test {
    static tests: any[];
    static runnable: boolean;
    static passed: number;
    static failed: number;
    static total: number;
    static fromUrl(url: any): Test;
    static run(): Promise<void>;
    static formatError(e: any): string;
    static log: (str: any) => boolean;
    name: any;
    passed: number;
    failed: number;
    cases: any[];
    message: string;
    constructor(name: any);
    add(name: any, fn: any): void;
    run(): Promise<void>;
}
//# sourceMappingURL=index.d.ts.map