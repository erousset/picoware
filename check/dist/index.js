// src/index.ts
import chalk from "chalk";
import * as path from "node:path";
var Test = class _Test {
  static tests = [];
  static runnable = false;
  static passed = 0;
  static failed = 0;
  static total = 0;
  static fromUrl(url) {
    return new _Test(path.basename(url, ".js"));
  }
  static async run() {
    for (const test of _Test.tests) {
      await test.run();
    }
    if (_Test.passed == _Test.total)
      console.log(chalk.green(`Passed: ${_Test.passed} / ${_Test.total}`));
    else {
      console.log(chalk.red(`Failed: ${_Test.failed} / ${_Test.total}`));
    }
  }
  static formatError(e) {
    const msg = e.stack;
    if (!msg)
      return chalk.yellow(e);
    const delimiter = msg.indexOf("\n");
    const title = msg.slice(0, delimiter);
    const stacktrace = msg.slice(delimiter);
    return `${chalk.yellow(title)} ${chalk.gray(stacktrace)}
`;
  }
  // can log from another process
  static log = (str) => process.stdout.write(str);
  name;
  passed = 0;
  failed = 0;
  cases = [];
  message = "";
  constructor(name) {
    _Test.tests.push(this);
    this.name = name;
  }
  add(name, fn) {
    this.cases.push({ name, fn });
    _Test.total++;
  }
  async run() {
    if (!_Test.runnable)
      return;
    _Test.log(chalk.cyan(this.name + " "));
    for (const test of this.cases) {
      let timeoutID = setTimeout(() => {
        this.message += chalk.red(`
! ${test.name} `);
        this.message += _Test.formatError("Test Timeout after 5000ms");
        _Test.log(chalk.red("\u2022 "));
        this.failed++;
        _Test.failed++;
      }, 5e3);
      try {
        await test.fn();
        clearTimeout(timeoutID);
        _Test.log(chalk.green("\u2022 "));
        this.passed++;
        _Test.passed++;
      } catch (e) {
        clearTimeout(timeoutID);
        this.message += chalk.red(`
! ${test.name} `);
        this.message += _Test.formatError(e);
        _Test.log(chalk.red("\u2022 "));
        this.failed++;
        _Test.failed++;
      }
    }
    if (this.passed == this.cases.length) {
      _Test.log(chalk.green(`\u2713
`));
    } else {
      _Test.log(chalk.red(`x
`));
    }
    if (this.message)
      _Test.log(this.message + "\n");
  }
};
export {
  Test
};
